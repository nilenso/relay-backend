FROM java:8-jre-alpine
WORKDIR /opt/relay-backend
COPY ./target/relay-backend-*-standalone.jar ./relay-backend-standalone.jar
RUN mkdir /etc/relay-vault && mkdir ~/.ssh && touch ~/.ssh/known_hosts
CMD ["java", "-XX:+UseG1GC", "-Dfile.encoding=UTF8", "-Xms512m", "-Xmx1g", "-Dsun.net.client.defaultConnectTimeout=60000", "-Dsun.net.client.defaultReadTimeout=60000", "-jar", "relay-backend-standalone.jar", "api"]
