(defproject relay-backend "0.1.0-SNAPSHOT"
  :description "Backend service for relay-chat"
  :url "https://gitlab.com/nilenso/relay-backend"
  :license {:name "AGPL-3.0"
            :url  "https://www.gnu.org/licenses/agpl-3.0.txt"}
  :dependencies [[amazonica "0.3.121"
                  :exclusions [com.google.guava/guava
                               com.google.protobuf/protobuf-java]]
                 [bidi "2.1.2"]
                 [bk/ring-gzip "0.2.1"]
                 [camel-snake-kebab "0.4.0"]
                 [cheshire "5.8.0"]
                 [cider/cider-nrepl "0.15.1"
                  :exclusions [org.clojure/tools.nrepl]]
                 [clj-http "3.7.0" :exclusions [commons-logging]]
                 [clj-ssh "0.5.14"]
                 [clojure.java-time "0.3.1"]
                 [clonfig "0.2.0"]
                 [commons-validator/commons-validator "1.6" :exclusions [commons-logging]]
                 [hikari-cp "2.0.0" :exclusions [prismatic/schema]]
                 [io.sentry/sentry-log4j2 "1.6.6"]
                 [io.sentry/sentry-clj "0.6.0"]
                 [honeysql "0.9.1"]
                 [medley "1.0.0"]
                 [mount "0.1.11"]
                 [nilenso/honeysql-postgres "0.2.3"]
                 [org.apache.logging.log4j/log4j-api "2.10.0"]
                 [org.apache.logging.log4j/log4j-core "2.10.0"]
                 [org.apache.logging.log4j/log4j-slf4j-impl "2.10.0" :exclusions [org.slf4j/slf4j-api]]
                 [org.clojure/clojure "1.9.0"]
                 [org.clojure/java.jdbc "0.7.4"]
                 [org.clojure/tools.logging "0.4.0"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [org.postgresql/postgresql "42.1.4"]
                 [org.slf4j/jcl-over-slf4j "1.7.25"]
                 [org.slf4j/slf4j-api "1.7.25"]
                 [ragtime "0.7.2"]
                 [ring/ring "1.6.3"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-defaults "0.3.1"]
                 [ring-hmac-check "0.2.1"]
                 [ring/ring-jetty-adapter "1.6.3"]
                 [ring/ring-json "0.4.0"]
                 [ring-logger "0.7.7"]

                 ;; clojurescript
                 [org.clojure/clojurescript "1.9.946"]
                 [org.clojure/core.async "0.3.465"]
                 [cljs-http "0.1.44"]
                 [reagent "0.7.0"]
                 [re-frame "0.10.2"]
                 [secretary "1.2.3"]
                 [kibu/pushy "0.3.8"]]


  :local-repo ".m2"
  :min-lein-version "2.5.3"
  :plugins [[lein-cljsbuild "1.1.5"]
            [deraen/lein-sass4clj "0.3.1"]]
  :source-paths ["src/clj"]
  :java-source-paths ["src"]
  :jvm-opts ["-server" "-XX:-OmitStackTraceInFastThrow"]
  :main ^:skip-aot relay-backend.core

  :clean-targets ^{:protect false} ["resources/public/js/compiled" "target"]
  :figwheel {:css-dirs ["resources/public/css"]}

  :profiles
  {:dev     {:dependencies [[binaryage/devtools "0.9.4"]
                            [re-frisk "0.5.3" :exclusions [com.cognitect/transit-cljs]]
                            [figwheel-sidecar "0.5.13"]
                            [com.cemerick/piggieback "0.2.2"]]
             :plugins      [[lein-figwheel "0.5.13"]
                            [com.jakemccrary/lein-test-refresh "0.22.0"]]
             :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
             :main         dev}

   :uberjar {:aot         :all
             :prep-tasks  ["javac"
                           "compile"
                           ;["sass4clj" "once"] -- put back once https://github.com/Deraen/sass4clj/issues/18 is fixed
                           ["cljsbuild" "once" "min"]]

             :main        relay_backend.main
             :global-vars {*warn-on-reflection* true}}
   :test    {:resource-paths ["resources", "test-resources"]}}

  :cljsbuild
  {:builds
   [{:id           "dev"
     :source-paths ["src/cljs"]
     :figwheel     {:on-jsload "relay-backend.core/mount-root"}
     :compiler     {:main                 relay-backend.core
                    :output-to            "resources/public/js/compiled/app.js"
                    :output-dir           "resources/public/js/compiled/out"
                    :asset-path           "/js/compiled/out"
                    :source-map-timestamp true
                    :preloads             [devtools.preload
                                           re-frisk.preload]
                    :external-config      {:devtools/config {:features-to-install :all}}}}


    {:id           "min"
     :source-paths ["src/cljs"]
     :jar          true
     :compiler     {:main            relay-backend.core
                    :externs         ["externs/grecaptcha.js"
                                      "externs/fastspring.js"]
                    :output-to       "resources/public/js/compiled/app.js"
                    :output-dir      "resources/public/js/compiled/min/out"
                    :asset-path      "js/compiled/min/out"
                    :optimizations   :advanced
                    :closure-defines {goog.DEBUG false}
                    :pretty-print    false}}]}

  :sass
  {:source-paths ["resources/sass"]
   :target-path  "resources/public/css"
   :output-style "compressed"
   :source-map   false})
