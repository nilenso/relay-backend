(ns dev
  (:require [clojure.tools.namespace.repl :refer [refresh]]
            [mount.core :as mount]
            [relay-backend.core]
            [relay-backend.nrepl-server]))

(defn reset []
  (mount/stop (mount/except [#'relay-backend.nrepl-server/server]))
  (mount.core/start))

(defn -main [& args]
  (apply relay-backend.core/-main args))
