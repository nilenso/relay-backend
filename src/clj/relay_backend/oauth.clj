;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.oauth
  (:require [camel-snake-kebab.core :as csk]
            [cheshire.core :as json]
            [clj-http.client :as http]
            [clj-time.core :as time]
            [clojure.string :as str]
            [crypto.random :as random]
            [relay-backend.config :as config]
            [relay-backend.db.core :as db]
            [relay-backend.db.org :as db-org]
            [relay-backend.instance :as instance]
            [relay-backend.util.map :as umap]
            [ring.util.codec :as codec]
            [ring.util.response :as resp]
            [relay-backend.notifications :as notify]))

(defn- random-state []
  (-> (random/base64 9) (str/replace "+" "-") (str/replace "/" "_")))

(defn- authorize-uri [{:keys [authorize-uri bot-client-id redirect-uri]} state]
  (str authorize-uri
       "?"
       (codec/form-encode {:response_type "code"
                           :client_id     bot-client-id
                           :redirect_uri  redirect-uri
                           :scope         ""
                           :state         state})))

(defn- format-access-token
  [{{:keys [access_token expires_in refresh_token id_token]} :body}]
  (-> {:token access_token}
      (cond->
        expires_in (assoc :expires (-> expires_in time/seconds time/from-now))
        refresh_token (assoc :refresh-token refresh_token)
        id_token (assoc :id-token id_token))))

(defn- request-params [profile request]
  {:grant_type   "authorization_code"
   :code         (get-in request [:query-params "code"])
   :redirect_uri (:redirect-uri profile)})

(defn- add-form-credentials [opts id secret]
  (assoc opts :form-params (-> (:form-params opts)
                               (merge {:client_id     id
                                       :client_secret secret}))))

(defn- get-access-token
  [{:keys [access-token-uri bot-client-id bot-client-secret] :as profile} request]
  (format-access-token
    (http/post access-token-uri
               (-> {:accept                :json
                    :as                    :json
                    :form-params           (request-params profile request)
                    :throw-entire-message? true}
                   (add-form-credentials bot-client-id bot-client-secret)))))

(defn- profile [{:keys [subdomain] :as org}]
  (assoc org
    :authorize-uri (format "https://%s/oauth/authorize" (instance/domain subdomain))
    :access-token-uri (format "https://%s/oauth/access_token" (instance/domain subdomain))
    :redirect-uri (config/oauth-callback-url subdomain)))

(defn- error-resp [status msg]
  {:status  status
   :headers {"Content-Type" "text/plain"}
   :body    msg})

(defn launch [{:keys [session route-params] :or {session {}} :as _request}]
  (let [subdomain (:subdomain route-params)
        state     (random-state)]
    (if-let [org (db/with-transaction [tx] (db-org/find tx {:subdomain subdomain}))]
      (-> (resp/redirect (authorize-uri (profile org) state))
          (assoc :session (assoc session :state state)))
      (error-resp 404 "Invalid subdomain"))))

(defn- state-mismatch? [request]
  (not= (get-in request [:session :state])
        (get-in request [:query-params "state"])))

(defn- access-denied? [request]
  (= (get-in request [:query-params "error"]) "access_denied"))

(defn get-user-info [subdomain access-token]
  (-> (http/get (instance/api-url subdomain "users/me")
                {:oauth-token (:token access-token)})
      :body
      (json/decode true)))

(defn- access-denied-error-msg []
  (format "Access denied. Please allow access to %s to login." (config/relay-server :host)))

(defn logged-in-session [session user-info org]
  (-> session
      (assoc :user (-> (umap/nested-map-keys csk/->kebab-case-keyword user-info)
                       (dissoc :notify-props)
                       (merge {:org-name (:name org) :org-id (:id org)})))
      (assoc :logged-in? true)
      (dissoc :state)))

(defn callback [{:keys [session route-params] :or {session {}} :as request}]
  (let [org (db/with-transaction [tx]
              (db-org/find tx {:subdomain (:subdomain route-params)}))]
    (cond
      (access-denied? request)
      (do (notify/info "Access Denied" :subdomain (:subdomain route-params))
          (resp/redirect (str "/login?error=" (access-denied-error-msg))))

      (state-mismatch? request)
      (do (notify/info "State mismatch" :subdomain (:subdomain route-params))
          (error-resp 400 "State mismatch"))

      (nil? org)
      (do (notify/info "Oauth callback: Invalid Subdomain" :subdomain (:subdomain route-params))
          (error-resp 404 "Invalid subdomain"))

      :else
      (let [access-token (get-access-token (profile org) request)
            user-info    (get-user-info (:subdomain route-params) access-token)]
        (-> (resp/redirect "/dashboard")
            (assoc :session (logged-in-session session user-info org)))))))
