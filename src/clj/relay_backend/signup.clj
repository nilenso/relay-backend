;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.signup
  (:require [cheshire.core :as json]
            [clj-http.client :as http]
            [clojure.spec.alpha :as s]
            [clojure.tools.logging :as log]
            [medley.core :refer [uuid random-uuid]]
            [relay-backend.config :as config]
            [relay-backend.host :as host]
            [relay-backend.db.core :as db]
            [relay-backend.db.org :as db-org]
            [relay-backend.instance :as instance]
            [relay-backend.spec :as spec]
            [relay-backend.notifications :as notify]
            [relay-backend.util.ring :refer [error-response]]))

(def captcha-url
  "https://www.google.com/recaptcha/api/siteverify")

(defn valid-captcha? [captcha-response]
  (let [response (-> (http/post
                       captcha-url
                       {:form-params {:secret   (config/captcha :secret-key)
                                      :response captcha-response}})
                     :body
                     (json/decode true))]
    (if (:success response)
      true
      (do (log/info :captcha-error-codes (:error-codes response))
          false))))

(defn update-org-status [id status]
  (db/with-transaction [tx]
    (db-org/update tx id {:status (get db-org/org-status status)})))

(defn signup [params]
  (if-let [{:keys [id subdomain]} (db/with-transaction [tx]
                                    (db-org/create tx (assoc params :status "signup")))]
    (if-let [host (host/select-host-to-deploy-org params)]
      (try
        (let [instance-status (if (instance/safely-spin-up host (assoc params :org-id id))
                                :running
                                :failed)]
          (case instance-status
            :running (notify/info (format "Signup successfully completed: %s" subdomain)
                                  :subodmain subdomain)
            :failed (notify/error (format "Signup failed: %s" subdomain)
                                  :subdomain subdomain))
          (update-org-status id instance-status))
        (catch Exception e
          (update-org-status id "failed")
          (log/error e "Error in spinning up instance for:" subdomain)
          (instance/spin-down host params))))))

(defn signup-org [{:keys [params] :as _req}]
  (cond
    (not (s/valid? ::spec/signup-params params))
    (do
      (log/info (str "Invalid signup params" (s/explain-str ::spec/signup-params params)))
      (error-response 422 "Invalid params"))

    (not (valid-captcha? (:captcha-response params)))
    (error-response 401 "Invalid Captcha")

    :else (let [org-id (random-uuid)]
            (future (signup (assoc params :id org-id)))
            {:status 202
             :body   {:id org-id}})))

(defn subdomain-available? [subdomain]
  (and (s/valid? ::spec/subdomain subdomain)
       (not (db/with-transaction [tx] (db-org/active? tx subdomain)))))

(defn subdomain-availability [{:keys [params]}]
  {:status 200
   :body   {:available (subdomain-available? (:subdomain params))}})
