;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db.org
  (:refer-clojure :exclude [update find list])
  (:require [clojure.string :as s]
            [honeysql.format :as sql]
            [honeysql.helpers :refer :all :exclude [update]]
            [relay-backend.db.core :as db]
            [relay-backend.db.honey :refer :all])
  (:import [java.time Instant]
           [org.postgresql.util PSQLException]))

(def org-status
  {:running "running"
   :signup  "signup"
   :failed  "failed"})

(defn list
  ([tx]
   (list tx {}))
  ([tx {:keys [id status subdomain statuses host-id org-id port limit]}]
   (->> (-> (select :*)
            (from :orgs)
            (where* (concat
                      (if id [[:= :id id]])
                      (if subdomain [[:= :subdomain (s/lower-case subdomain)]])
                      (if status [[:= :status status]])
                      (if host-id [[:= :host-id host-id]])
                      (if org-id [[:= :org-id org-id]])
                      (if port [[:= :port port]])
                      (if (seq statuses) [[:in :status statuses]])))
            (limit* limit))
        (sql/format)
        (db/q tx))))

(defn find [tx options]
  (first (list tx (assoc options :limit 1))))

(defn active? [tx subdomain]
  (some?
    (find tx {:subdomain subdomain
              :statuses  [(:running org-status)
                          (:signup org-status)]})))

(defn create [tx {:keys [subdomain] :as params}]
  (try
    (when-not (active? tx subdomain)
      (db/create tx :orgs (-> params
                              (select-keys [:id :name :subdomain :status])
                              (merge {:created-at (Instant/now)
                                      :updated-at (Instant/now)}))))
    (catch PSQLException e
      ;; NOTE: the below SQLState is for unique constraint violation
      (if (= (.getSQLState e) "23505")
        nil
        (throw e)))))

(defn update [tx org-id patch]
  (db/update tx :orgs
             (-> (select-keys patch [:status
                                     :bot-client-id
                                     :bot-client-secret
                                     :host-id
                                     :port
                                     :target-group-arn
                                     :listener-rule-arn])
                 (assoc :updated-at (Instant/now)))
             ["id = ?" org-id]))
