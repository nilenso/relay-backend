;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db.charge-attempt
  (:refer-clojure :exclude [update list find partition-by])
  (:require [java-time :as jt]
            [honeysql.format :as sql]
            [honeysql.helpers :refer :all :exclude [update]]
            [medley.core :refer [random-uuid]]
            [relay-backend.db.core :as db]
            [relay-backend.db.honey :refer :all]
            [honeysql.format :as sql]))

(def required-keys
  [:org-id :subs-id :active-user-count :amount :period-start :period-end :status])

(def update-keys
  [:order-id :status :payment-id :failure-reason])

(def charge-attempt-status
  {:attempted      "attempted"
   :request-failed "request-failed"
   :failed         "failed"
   :success        "success"})

(defn create [tx charge-attempt]
  (let [now (jt/instant)]
    (db/create tx :charge-attempts
               (-> charge-attempt
                   (select-keys required-keys)
                   (merge {:id         (random-uuid)
                           :created-at now
                           :updated-at now})))))

(defn update [tx charge-attempt]
  (db/update tx :charge-attempts
             (-> charge-attempt
                 (select-keys update-keys)
                 (merge {:updated-at (jt/instant)}))
             ["id = ?" (:id charge-attempt)]))

(defn charge-attempts-for-org-subs [tx org-id subs-id]
  (let [charge-attempts
        (->> (-> (select :ca.*)
                 (from [:charge-attempts :ca] [:subscriptions :subs])
                 (where [:and
                         [:= :ca.org-id org-id]
                         [:= :ca.status (:attempted charge-attempt-status)]
                         [:= :ca.subs-id :subs.id]
                         [:= :subs.subs-id subs-id]
                         [:= :subs.org-id org-id]]))
             (sql/format)
             (db/q tx))]
    (if (> (count charge-attempts) 1)
      (throw (ex-info "More than one charge attempts found." {:org-id org-id :subs-id subs-id}))
      (first charge-attempts))))

(defn list
  ([tx]
   (list tx {}))
  ([tx {:keys [id org-id subs-id status limit]}]
   (->> (-> (select :*)
            (from :charge-attempts)
            (where* (concat
                      (if id [[:= :id id]])
                      (if org-id [[:= :org-id org-id]])
                      (if subs-id [[:= :subs-id subs-id]])
                      (if status [[:= :status status]])))
            (limit* limit))
        (sql/format)
        (db/q tx))))

(defn find [tx options]
  (first (list tx (assoc options :limit 1))))
