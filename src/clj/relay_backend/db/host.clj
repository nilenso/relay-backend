;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db.host
  (:refer-clojure :exclude [list find])
  (:require [honeysql.format :as sql]
            [honeysql.helpers :refer :all :exclude [update]]
            [relay-backend.db.core :as db]
            [relay-backend.db.honey :refer :all])
  (:import [org.postgresql.util PSQLException]
           [java.time Instant]))

(defn create [tx options]
  (try
    (let [now (Instant/now)]
      (db/create tx :hosts (-> options
                               (select-keys [:id :instance-id :private-ipaddr :capacity])
                               (assoc :created-at now :updated-at now))))
    (catch PSQLException e
      ;; NOTE: the below SQLState is for unique constraint violation
      (if (= (.getSQLState e) "23505")
        nil
        (throw e)))))

(defn list
  ([tx]
   (list tx {}))
  ([tx {:keys [id instance-id private-ipaddr limit]}]
   (->> (-> (select :*)
            (from :hosts)
            (where* (concat
                      (if id [[:= :id id]])
                      (if instance-id [[:= :instance-id instance-id]])
                      (if private-ipaddr [[:= :private-ipaddr private-ipaddr]])))
            (limit* limit))
        (sql/format)
        (db/q tx))))

(defn find
  ([tx]
   (find tx {}))
  ([tx options]
   (first (list tx (assoc options :limit 1)))))

(defn host-with-least-number-of-orgs [tx]
  (let [selected-host
        (->> (-> (select :hosts.* [:%count.orgs.id :num-orgs] [:%array_agg.orgs.port :occupied-ports])
                 (from :hosts)
                 (left-join :orgs [:= :hosts.id :orgs.host-id])
                 (group :hosts.id)
                 (having [:< :%count.orgs.id :hosts.capacity])
                 (order-by :num-orgs)
                 (limit 1))
             sql/format
             (db/q tx)
             first)]
    (update-in selected-host [:occupied-ports] #(remove nil? %))))