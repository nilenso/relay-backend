;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db.core
  (:refer-clojure :exclude [update])
  (:require [relay-backend.config :refer [config]]
            [camel-snake-kebab.core :as csk]
            [cheshire.core :as json]
            [clojure.java.jdbc :as jdbc]
            [clojure.tools.logging :as log]
            [hikari-cp.core :as hikari]
            [medley.core :refer [map-keys]]
            [mount.core :refer [defstate]])
  (:import [clojure.lang IPersistentMap IPersistentVector]
           [java.io Writer]
           [java.sql PreparedStatement Timestamp]
           [java.time Instant ZoneId]
           [java.util Calendar TimeZone]
           [org.postgresql.jdbc PgArray]
           [org.postgresql.util PGobject]))

(defstate datasource
  :start (do (log/info "Starting DB connection pool")
             (hikari/make-datasource (:db config)))
  :stop (do (hikari/close-datasource datasource)
            (log/info "Closed DB connection pool")))

(defn- ->pg-object [^String type ^String value]
  (doto (PGobject.)
    (.setType type)
    (.setValue value)))

(defn- clj->jsonb-pg-object [x]
  (->pg-object "jsonb" (if (instance? IPersistentMap x)
                         (json/encode x)
                         x)))

(defn- jsonb-pg-object->clj [^PGobject pg-obj]
  (json/decode (.getValue pg-obj)))

;; Makes sure that timestamps in the DB and app are always in UTC
(extend-protocol jdbc/IResultSetReadColumn
  Timestamp
  (result-set-read-column [^Timestamp v _ _]
    (-> v (.toLocalDateTime) (.atZone (ZoneId/of "UTC")) (.toInstant)))

  PgArray
  (result-set-read-column [arr _ _]
    (vec (.getArray ^PgArray arr)))

  PGobject
  (result-set-read-column [pg-obj _ _]
    (case (.getType pg-obj)
      "jsonb" (jsonb-pg-object->clj pg-obj)
      pg-obj)))

(extend-protocol jdbc/ISQLParameter
  Instant
  (set-parameter [^Instant v ^PreparedStatement stmt ^long idx]
    (.setTimestamp stmt idx (Timestamp/from v) (Calendar/getInstance (TimeZone/getTimeZone "UTC"))))

  IPersistentVector
  (set-parameter [v ^PreparedStatement stmt ^long i]
    (let [conn      (.getConnection stmt)
          meta      (.getParameterMetaData stmt)
          type-name (.getParameterTypeName meta i)]
      (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
        (.setObject stmt i (.createArrayOf conn elem-type (to-array v)))
        (.setObject stmt i v))))

  IPersistentMap
  (set-parameter [^IPersistentMap v ^PreparedStatement stmt ^long idx]
    (.setObject stmt idx (clj->jsonb-pg-object v))))

(defmacro with-transaction [[transaction] & body]
  `(jdbc/with-db-transaction
     [~transaction {:datasource datasource}]
     ~@body))

(defn rollback-tx [tx]
  (jdbc/db-set-rollback-only! tx))

(defmethod print-method Instant [^Instant inst ^Writer w]
  (.write w (str inst)))

(def ^:private keyword->colname (fn [args] (csk/->snake_case_string args :separator \-)))

(def ^:private colname->keyword (fn [args] (csk/->kebab-case-keyword args :separator \_)))

(defn- record->map [record]
  (map-keys #(-> % name colname->keyword) record))

(defn create [tx table row-map]
  (->> (jdbc/insert! tx table row-map {:entities     keyword->colname
                                       :transaction? false})
       first
       record->map))

(defn update [tx table row-map where-clause]
  (jdbc/update! tx
                table
                (assoc row-map :updated-at (Instant/now))
                where-clause
                {:entities     keyword->colname
                 :transaction? false})
  nil)

(defn q [tx query]
  (jdbc/query tx query {:row-fn record->map}))

(defn truncate [tx table]
  (jdbc/execute! tx [(str "TRUNCATE " table)] {:transaction? false}))

(defn execute! [tx query]
  (jdbc/execute! tx query {:transaction? false}))
