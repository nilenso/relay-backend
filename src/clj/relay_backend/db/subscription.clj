;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db.subscription
  (:refer-clojure :exclude [update list find partition-by])
  (:require [honeysql.format :as sql]
            [honeysql.helpers :refer :all]
            [honeysql-postgres.format :as psql]
            [honeysql-postgres.helpers :refer :all]
            [medley.core :refer (random-uuid)]
            [relay-backend.db.core :as db]
            [relay-backend.db.honey :refer :all]
            [clojure.set :as set]
            [java-time :as jt]))

(def ^:private required-keys
  #{:org-id :subs-id :status :currency :created-at :updated-at})

(def ^:private update-keys
  (set/difference required-keys #{:org-id :subs-id :created-at}))

(defn create [tx subscription]
  (let [now          (jt/instant)
        subscription (-> subscription
                         (select-keys required-keys)
                         (assoc :id (random-uuid)
                                :created-at now
                                :updated-at now))
        query        (-> (insert-into-as :subscriptions :s)
                         (values [subscription])
                         (upsert (-> (on-conflict-constraint :unique_org_subs)
                                     (do-update-set* update-keys)
                                     (where [:and
                                             [:= :s.org-id (:org-id subscription)]
                                             [:= :s.subs-id (:subs-id subscription)]])))
                         (returning :s.*))]
    (->> query
         (sql/format)
         (db/q tx)
         (first))))

(defn list
  ([tx]
   (list tx {}))
  ([tx {:keys [id org-id subs-id status limit]}]
   (->> (-> (select :*)
            (from :subscriptions)
            (where* (concat
                      (if id [[:= :id id]])
                      (if org-id [[:= :org-id org-id]])
                      (if subs-id [[:= :subs-id subs-id]])
                      (if status [[:= :status status]])))
            (limit* limit))
        (sql/format)
        (db/q tx))))

(defn find [tx options]
  (first (list tx (assoc options :limit 1))))
