;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db.payment
  (:refer-clojure :exclude [find list partition-by])
  (:require [java-time :as jt]
            [honeysql.format :as sql]
            [honeysql.helpers :refer :all :exclude [update]]
            [honeysql-postgres.format :refer :all]
            [honeysql-postgres.helpers :refer :all]
            [medley.core :refer (random-uuid)]
            [relay-backend.db.core :as db]
            [relay-backend.db.honey :refer :all]
            [clojure.set :as set]))

(defn list
  ([tx]
   (list tx {}))
  ([tx {:keys [id order-id completed limit created-after created-before]}]
   (->> (-> (select :*)
            (from :payments)
            (where* (concat
                      (if id [[:= :id id]])
                      (if order-id [[:= :order-id order-id]])
                      (if-not (nil? completed) [[:= :completed completed]])
                      (if created-after [[:>= :created-at created-after]])
                      (if created-before [[:<= :created-at created-before]])))
            (limit* limit))
        (sql/format)
        (db/q tx))))

(defn find [tx options]
  (first (list tx (assoc options :limit 1))))

(def ^:private required-keys
  #{:order-id :org-id :subs-id
    :charge-total :charge-tax :charge-currency
    :payout-total :payout-tax :payout-currency :completed :invoice-url
    :created-at :updated-at})

(def ^:private update-keys
  (set/difference required-keys
                  #{:order-id :org-id :subs-id :created-at}))

(defn create [tx {:keys [order-id subs-id org-id] :as payment}]
  (let [payment (-> payment
                    (select-keys required-keys)
                    (assoc :id (random-uuid)
                           :updated-at (jt/instant)))
        query   (-> (insert-into-as :payments :p)
                    (values [payment])
                    (upsert (-> (on-conflict-constraint :unique_org_subs_order)
                                (do-update-set* update-keys)
                                (where [:and
                                        [:= :p.order-id order-id]
                                        [:= :p.subs-id subs-id]
                                        [:= :p.org-id org-id]])))
                    (returning :p.*))]
    (->> query
         (sql/format)
         (db/q tx)
         (first))))
