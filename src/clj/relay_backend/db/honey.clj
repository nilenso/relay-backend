;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db.honey
  (:refer-clojure :exclude [update partition-by])
  (:require [honeysql.format :as sql]
            [honeysql.helpers :refer :all]
            [honeysql-postgres.format :refer :all]
            [honeysql-postgres.helpers :refer :all]))

;; sql helpers and utils

(defn where*
  ([args]
   (where* {} args))
  ([m args]
   (if-not (empty? args)
     (apply where (list* m args))
     m)))

(defn select*
  ([args]
   (select* {} args))
  ([m args]
   (if-not (empty? args)
     (apply select (list* m args))
     m)))

(defn limit*
  ([arg]
   (limit* {} arg))
  ([m arg]
   (if-not (nil? arg)
     (limit m arg)
     m)))

(defn do-update-set*
  ([args]
   (do-update-set* {} args))
  ([m args]
   (if-not (empty? args)
     (apply do-update-set m args)
     m)))

(defn format-predicate [pred]
  (binding [sql/*parameterizer* (constantly "?")]
    (sql/format-predicate pred)))

(defmethod sql/fn-handler "&&" [_ array-1 array-2]
  (str (sql/to-sql array-1) " && " (sql/to-sql array-2)))
