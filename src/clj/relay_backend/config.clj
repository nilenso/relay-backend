;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.config
  (:refer-clojure :exclude [delay])
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [clonfig.core :as clonfig]
            [medley.core :refer :all :exclude [boolean? uuid?]]
            [mount.core :refer [defstate]]
            [clojure.string :as str]))

(defn redact-keys [config ks]
  (reduce dissoc-in config ks))

(defn make-config [config-file]
  (let [config (-> config-file
                   (io/resource)
                   (slurp)
                   (edn/read-string)
                   (clonfig/read-config))]
    (log/info "Loaded config from file:" config-file)
    (log/debug "Config:\n" (with-out-str
                             (-> config
                                 (redact-keys [[:db :password]
                                               [:captcha :secret-key]
                                               [:relay-server :username]
                                               [:relay-server :private-key-path]
                                               [:relay-server :bot-username]
                                               [:relay-server :bot-password]
                                               [:relay-server :bot-email]
                                               [:relay-server :bot-email-password]
                                               [:fastspring :api-credentials]
                                               [:fastspring :webhook-secret]
                                               [:sentry :dsn]])
                                 (clojure.pprint/pprint))))
    config))

(defstate config
  :start (make-config "config.edn"))

(defn db-jdbc-uri []
  (let [{:keys [server-name port-number database-name username password adapter]} (:db config)]
    (format "jdbc:%s://%s:%s/%s?user=%s&password=%s"
            adapter server-name port-number database-name username password)))

(defn env [] (:env config))

(defn dev-env? []
  (= :dev (:env config)))

(defn test-env? []
  (= :test (:env config)))

(defn prod-env? []
  (= :prod (:env config)))

(defmacro def-accessor [key]
  (let [k (symbol (name key))]
    `(defn ~k
       ([] (~key config))
       ([& keys#] (get-in (~k) keys#)))))

(def-accessor :fastspring)
(def-accessor :relay-server)
(def-accessor :subscription)
(def-accessor :captcha)
(def-accessor :sentry)
(def-accessor :amazon)

(defn relay-server-command [key]
  (relay-server :commands key))

(defn oauth-callback-url [subdomain]
  (let [backend-url       (relay-server :backend-url)
        callback-host-url (if-not (str/blank? backend-url)
                            backend-url
                            (format "https://%s" (relay-server :host)))]
    (format "%s/oauth/%s/callback" callback-host-url subdomain)))
