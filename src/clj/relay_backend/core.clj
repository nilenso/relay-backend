;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.core
  (:require [relay-backend.config :as config]
            [relay-backend.db-migration :as db-migration]
            [relay-backend.nrepl-server]
            [relay-backend.server]
            [relay-backend.subscription :as subs]
            [relay-backend.util.map :as umap]
            [clojure.string :as string]
            [clojure.tools.logging :as log]
            [mount.core :as mount]
            [relay-backend.host :as host]
            [relay-backend.db.core :as db]))

(defn- add-shutdown-hook [service-name]
  (.addShutdownHook
    (Runtime/getRuntime)
    (Thread. ^Runnable #(do (mount/stop)
                            (shutdown-agents)
                            (log/info "Stopped" service-name)
                            (log/info "Stopped Relay Backend"))
             "Shutdown-handler")))

(defmacro start-service [service-name & body]
  `(try
     (add-shutdown-hook ~service-name)
     (mount/stop)
     (log/info "Starting Relay Backend")
     (mount/start #'config/config)
     (when (:run-migrations config/config)
       (db-migration/migrate))
     ~@body
     (log/info "Started" ~service-name)
     (catch Exception e#
       (log/error e# "Error in starting" ~service-name)
       (mount/stop)
       (System/exit 1))))

(defn- run-job [job-name job-fn]
  (try
    (add-shutdown-hook (str job-name " job"))
    (job-fn)
    (System/exit 0)
    (catch Exception e
      (log/error e "Error while running job")
      (System/exit 1))))

(defn- run-migrate []
  (mount/start #'config/config)
  (db-migration/migrate))

(defn- run-rollback [args]
  (mount/start #'config/config)
  (->> args
       (apply hash-map)
       (umap/nested-map-keys keyword)
       (db-migration/rollback)))

(defn- setup-existing-routes [args]
  (mount/start #'config/config #'db/datasource)
  (let [orgs        (-> args (second) (slurp) (clojure.edn/read-string))
        instance-id (first args)]
    (host/setup-existing-orgs orgs instance-id)))

(defn- create-host [args]
  (mount/start #'config/config #'db/datasource)
  (let [instance-id    (first args)
        private-ipaddr (second args)
        capacity       (Integer/parseInt (nth args 2))]
    (host/create-host instance-id private-ipaddr capacity)))

(defn- start-api []
  (start-service "API service"
    (mount/start)))

(def ^:private usage
  (->> [""
        "Usage:"
        "java -jar relay-backend-<version>-standalone.jar <command>"
        "OR"
        "lein trampoline run <command>"
        ""
        "Commands:"
        "api                                                   Starts the API server"
        "migrate                                               Runs the new migrations"
        "rollback                                              Rolls back the last run migration"
        "rollback last <n>                                     Rolls back last n migrations"
        "rollback to <name>                                    Rolls to a specific migration"
        "setup-existing-routes <instance-id> <filepath>        Setup routing for existing orgs"
        "create-host <instance-id> <private-ipaddr> <capacity> Create a new entry for hosts"
        "help                                                  Prints the help"]
       (string/join \newline)))

(defn- print-usage [msg]
  (println msg)
  (println usage))

(defn -main [& [command & args]]
  (case command
    "api" (start-api)
    "migrate" (run-job "migrate" run-migrate)
    "rollback" (run-job "rollback" #(run-rollback args))
    "setup-existing-routes" (run-job "setup-existing-routes" #(setup-existing-routes args))
    "create-host" (run-job "create-host" #(create-host args))
    "help" (do (print-usage "Backend for relay-chat.com")
               (System/exit 0))
    (do
      (print-usage "Must supply a valid command to run")
      (System/exit 1))))
