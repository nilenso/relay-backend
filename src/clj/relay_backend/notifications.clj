;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.notifications
  (:require [clojure.string :as s]
            [mount.core :refer [defstate]]
            [relay-backend.config :as config]
            [sentry-clj.core :as sentry]))

(defn init-sentry []
  (let [dsn (config/sentry :dsn)]
    (when-not (s/blank? dsn)
      (sentry/init! (format "%s?environment=%s&stacktrace.app.packages=relay_backend" dsn (name (config/env)))))))

(defstate sentry-client
  :start (init-sentry))

(defn send-to-sentry [level message & {:as extra}]
  (when sentry-client
    (sentry/send-event
      {:environment (name (config/env))
       :level       level
       :message     message
       :tags        {:custom true}
       :extra       extra})))

(def info (partial send-to-sentry :info))
(def warning (partial send-to-sentry :warning))
(def error (partial send-to-sentry :error))
(def fatal (partial send-to-sentry :fatal))
