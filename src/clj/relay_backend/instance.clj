;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.instance
  (:require [cheshire.core :as json]
            [clj-http.client :as http]
            [clj-ssh.ssh :as ssh]
            [clojure.tools.logging :as log]
            [relay-backend.config :as config]
            [relay-backend.db.core :as db]
            [relay-backend.db.org :as db-org]
            [relay-backend.util.retry :refer [with-retry*]]
            [relay-backend.host :as host]))

(defn command [subdomain command-key]
  (format "ENV=%s CLIENT_ID=%s %s"
          (name (config/env))
          subdomain
          (config/relay-server-command command-key)))

(defn domain [subdomain]
  (format "%s.%s"
          subdomain
          (config/relay-server :host)))

(defn api-url [subdomain path]
  (format "https://%s/api/v4/%s" (domain subdomain) path))

(defn active? [subdomain]
  (with-retry*
    (config/relay-server :active-check-count)
    2000
    #(-> (api-url subdomain "system/ping")
         (http/get {:insecure? true})
         (:body)
         (json/decode true)
         (:status)
         (= "OK"))))

(defn trigger-verify-mail [subdomain email]
  (log/debug "Triggering verify email for:" subdomain)
  (-> (http/post
        (api-url subdomain "users/email/verify/send")
        {:form-params           {"email" email}
         :content-type          :json
         :accept                :json
         :insecure?             true
         :throw-entire-message? true})
      (:body)
      (json/decode true)
      (:status)
      (= "OK")))

(defn ssh-run [{:keys [private-ipaddr]} command]
  (let [agent (ssh/ssh-agent {:use-system-ssh-agent false})
        {:keys [private-key-path username]} (config/relay-server)]
    (ssh/add-identity agent {:private-key-path private-key-path})
    (let [session (ssh/session agent private-ipaddr
                               {:username username :strict-host-key-checking :no})]
      (ssh/with-connection session
        (log/debug "Running SSH command:" command)
        (let [result (ssh/ssh session {:in command})]
          (or (zero? (:exit result))
              (do (log/error "Error in running ssh command. Output:" result)
                  false)))))))

(defn create-user-command [{:keys [username email password subdomain]}]
  (format "USERNAME=%s EMAIL=%s PASSWORD=%s %s"
          username email password
          (command subdomain :create-relay-user)))

(defn delete-user-command [{:keys [email subdomain]}]
  (format "EMAIL=%s %s" email (command subdomain :delete-relay-user)))

(defn create-org-command [subdomain selected-port]
  (format "PORT=%s %s" selected-port (command subdomain :create-instance)))

(defn login-bot [subdomain]
  (log/debug "Logging in as bot for:" subdomain)
  (let [{:keys [bot-username bot-password]} (config/relay-server)]
    (-> (http/post (api-url subdomain "users/login")
                   {:insecure?             true
                    :form-params           {:login_id bot-username :password bot-password}
                    :content-type          :json
                    :throw-entire-message? true
                    :cookie-policy         :standard})
        :headers
        (get "Token"))))

(defn register-oauth-app [subdomain]
  (let [oauth-token (login-bot subdomain)
        host        (config/relay-server :host)
        host-url    (fn [path] (str "https://" host path))]
    (log/debug "Registering" host "as oauth app for:" subdomain)
    (-> (http/post
          (api-url subdomain "oauth/apps")
          {:form-params           {"name"          host
                                   "description"   "Workplace Messaging. Hosted. Affordable. Open Source."
                                   "icon_url"      (host-url "/android-icon-192x192.png")
                                   "callback_urls" [(config/oauth-callback-url subdomain)]
                                   "homepage"      (host-url "")
                                   "is_trusted"    true}
           :content-type          :json
           :accept                :json
           :oauth-token           oauth-token
           :insecure?             true
           :throw-entire-message? true})
        (:body)
        (json/decode true))))

(defn create-bot [host org-id subdomain]
  (let [command (format "CLIENT_ID=%s BOT_PASSWORD=%s %s"
                        subdomain
                        (config/relay-server :bot-password)
                        (command subdomain :create-bot-user))]
    (log/info "creating bot user for:" subdomain)
    (if (ssh-run host command)
      (let [response (register-oauth-app subdomain)]
        (db/with-transaction [tx]
          (db-org/update tx
                         org-id
                         {:bot-client-id     (:id response)
                          :bot-client-secret (:client_secret response)}))
        (log/info "Created bot user and registered oauth app for:" subdomain)
        true)
      (do
        (log/error "Unable to run create bot user for:" subdomain)
        false))))

(defn spin-up [host {:keys [org-id email subdomain] :as params}]
  (try
    (log/info "Spinning up instance for:" subdomain)
    (and (ssh-run host (create-org-command subdomain (:selected-port host)))
         (host/setup-routing host params)
         (active? subdomain)
         (ssh-run host (create-user-command params))
         (create-bot host org-id subdomain)
         (trigger-verify-mail subdomain email))
    (catch Exception e
      (log/error e "Error in spinning up instance for:" subdomain)
      false)))

(defn spin-down [host {:keys [subdomain] :as params}]
  (try
    (log/info "Spinning down instance for:" subdomain)
    (ssh-run host (delete-user-command params))
    (ssh-run host (command subdomain :remove-instance))
    (host/teardown-routing params)
    false
    (catch Exception e
      (log/error e "Error in spinning down instance for:" subdomain)
      false)))

(defn safely-spin-up [host params]
  (or (spin-up host params)
      (spin-down host params)))
