;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.server.routes
  (:require [bidi.ring :as bidi]
            [relay-backend.fastspring :as fastspring]
            [relay-backend.oauth :as oauth]
            [relay-backend.org :as org]
            [relay-backend.server.middleware :as m]
            [relay-backend.signup :as signup]
            [relay-backend.user :as user]
            [ring.logger :refer [wrap-with-logger]]
            [ring.util.response :as ring-resp]))

(defn- ping [_request]
  {:status  200
   :headers {"Content-Type" "text/plain"}
   :body    "pong"})

(defn- not-found-handler [_req] (ring-resp/not-found ""))

(defn api-routes []
  [["v1/" {"ping"                   ping
           "orgs"                   {["/" :id] {:get org/get}}
           "subdomain-availability" {:get signup/subdomain-availability}
           "users"                  {"/me" {:get    (m/auth-middleware user/me)
                                            :delete (m/auth-middleware user/logout)}}}]
   [true not-found-handler]])

(defn webhook-routes []
  {"fastspring" (m/fastspring-webhook-middleware fastspring/handle-events)})

(defn oauth-routes []
  {[:subdomain ""]          oauth/launch
   [:subdomain "/callback"] oauth/callback})

(defn default-site-handler [_req]
  (assoc
    (ring-resp/resource-response "app.html" {:root "public"})
    :headers {"Content-Type" "text/html"}))

(defn routes []
  ["/" [["api/" (bidi/wrap-middleware (api-routes) m/api-middleware)]
        ["oauth/" (bidi/wrap-middleware (oauth-routes) m/oauth-middleware)]
        ["webhooks/" (webhook-routes)]
        ["downloads/" (fn [_] (ring-resp/redirect "/#downloads"))]
        [true (m/site-middleware default-site-handler)]]])

(defn make-handler []
  (-> (routes)
      (bidi/make-handler)
      (wrap-with-logger {:printer    :no-color
                         :exceptions false})
      (m/wrap-errors)))
