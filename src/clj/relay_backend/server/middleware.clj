;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.server.middleware
  (:require [cheshire.core :as json]
            [clj-stacktrace.repl :as st]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.logging :as log]
            [relay-backend.config :as config]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.defaults :as ring-defaults]
            [ring.middleware.gzip :refer [wrap-gzip]]
            [ring.middleware.json :refer [wrap-json-params wrap-json-response]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.session.memory :as session-memory]
            [ring.util.response :as ring-resp]
            [ring.middleware.hmac-check :as hmac-check])
  (:import [java.util Base64]))

(defn wrap-default-content-type-json [handler]
  (fn [request]
    (let [response     (handler request)
          content-type (ring-resp/get-header response "content-type")]
      (if (or (nil? content-type) (str/starts-with? content-type "application/octet-stream"))
        (ring-resp/content-type response "application/json; charset=utf-8")
        response))))

(defn wrap-errors [handler]
  (fn [request]
    (try
      (handler request)
      (catch Exception ex
        (log/error ex "Uncaught error in server")
        {:status 500 :body (json/encode {:Error (st/pst-str ex)})}))))

(defn indexify-uri [uri]
  (letfn [(has-no-extn? [u] (-> u (str/split #"/") (last) (str/includes? ".") (not)))]
    (cond
      (str/ends-with? uri "/") (str uri "index.html")
      (has-no-extn? uri) (str uri "/index.html")
      :else uri)))

(defn wrap-dir-index [handler]
  (fn [req]
    (handler (update-in req [:uri] indexify-uri))))

(defn auth-middleware [handler]
  (fn [{:keys [session] :as req}]
    (if (:logged-in? session)
      (handler req)
      {:status 401})))

(defonce session-store (session-memory/memory-store))

(def api-defaults
  (-> (if (config/prod-env?)
        ring-defaults/secure-api-defaults
        ring-defaults/api-defaults)
      (assoc :cookies true
             :session {:cookie-attrs {:http-only true, :same-site :strict}
                       :store        session-store})))

(defn api-middleware [handler]
  (-> handler
      (ring-defaults/wrap-defaults api-defaults)
      (wrap-json-params)
      (wrap-json-response)
      (wrap-default-content-type-json)))

(def site-defaults
  (-> (if (config/prod-env?)
        ring-defaults/secure-site-defaults
        ring-defaults/site-defaults)
      (assoc-in [:session :cookie-attrs :same-site] :lax)
      (assoc-in [:session :store] session-store)))

(defn site-middleware [handler]
  (-> handler
      (wrap-resource "public")
      (wrap-content-type)
      (wrap-dir-index)
      (ring-defaults/wrap-defaults site-defaults)
      (wrap-gzip)))

(defn oauth-middleware [handler]
  (-> handler
      (ring-defaults/wrap-defaults site-defaults)))

(defn body-as-string [request]
  (let [body (or (:cached-body request) (:body request))]
    (if (string? body)
      body
      (slurp body))))

(defn wrap-dup-body [handler]
  (fn [req]
    (let [body (body-as-string req)]
      (handler (assoc req :body (io/input-stream (.getBytes body))
                          :cached-body body)))))

(defn fastspring-webhook-middleware [handler]
  (-> handler
      (api-middleware)
      (hmac-check/wrap-hmac-check
        {:algorithm      "HmacSHA256"
         :header-field   "x-fs-signature"
         :secret-key     (config/fastspring :webhook-secret)
         :message        :cached-body
         :digest-decoder (fn [digest] (.decode (Base64/getDecoder) (.getBytes digest)))})
      (wrap-dup-body)))
