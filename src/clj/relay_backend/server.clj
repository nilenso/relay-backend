;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.server
  (:require [relay-backend.config :refer [config]]
            [relay-backend.server.routes :as routes]
            [cheshire.generate :refer [add-encoder encode-str]]
            [clojure.tools.logging :as log]
            [mount.core :refer [defstate]]
            [ring.adapter.jetty :as ring-jetty])
  (:import (org.eclipse.jetty.server Server)
           (java.time Instant)))

(add-encoder Instant encode-str)

(defn- start [handler]
  (let [conf         (:http-server config)
        port         (:port conf)
        thread-count (:thread-count conf)]
    (log/info "Starting server on port:" port)
    (ring-jetty/run-jetty handler {:port                 port
                                   :min-threads          thread-count
                                   :max-threads          thread-count
                                   :join?                false
                                   :send-server-version? false})))

(defn- stop [^Server server]
  (.stop server)
  (log/info "Stopped server"))

(defstate server
  :start (start (routes/make-handler))
  :stop (stop server))
