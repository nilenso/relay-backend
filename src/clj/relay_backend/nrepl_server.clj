;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.nrepl-server
  (:require [relay-backend.config :refer [config]]
            [cider.nrepl :refer (cider-nrepl-handler)]
            [clojure.tools.logging :as log]
            [clojure.tools.nrepl.server :as nrepl]
            [mount.core :refer [defstate]]))

(defn- start []
  (let [port (-> config :nrepl-server :port)]
    (log/info "Starting nREPL server on port:" port)
    (nrepl/start-server :port port :handler cider-nrepl-handler)))

(defn- stop [server]
  (nrepl/stop-server server)
  (log/info "Stopped nREPL server"))

(defstate server
  :start (start)
  :stop (stop server))
