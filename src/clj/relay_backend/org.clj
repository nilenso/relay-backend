;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.org
  (:refer-clojure :exclude [get])
  (:require [clojure.spec.alpha :as s]
            [java-time :as jt]
            [medley.core :refer [uuid]]
            [relay-backend.db.core :as db]
            [relay-backend.db.org :as db-org]
            [relay-backend.db.subscription :as db-subs]
            [relay-backend.instance :as instance]
            [relay-backend.spec :as spec]
            [relay-backend.util.ring :refer [error-response]]))

(defn days-left [{:keys [created-at] :as _org}]
  (- 30 (jt/time-between created-at (jt/instant) :days)))


(defn get [{:keys [route-params session] :as _req}]
  (let [org-id (:id route-params)]
    (if-not (s/valid? ::spec/org-id org-id)
      (error-response 422 "Invalid params")
      (db/with-transaction [tx]
        (if-let [org (db-org/find tx {:id (uuid org-id)})]
          (let [resp (-> org
                         (select-keys [:id :name :subdomain :status])
                         (assoc :domain (instance/domain (:subdomain org))))
                resp (cond-> resp
                       (:logged-in? session)
                       (assoc :subscription (db-subs/find tx {:org-id (:id org)
                                                              :status "active"})
                              :days-left (days-left org)))]
            {:status 200
             :body   resp})
          (error-response 404 "Not Found"))))))
