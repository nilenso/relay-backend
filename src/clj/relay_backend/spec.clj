;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.spec
  (:require [clojure.spec.alpha :as s]
            [relay-backend.config :as config]
            [clojure.string :as str])
  (:import [org.apache.commons.validator.routines DomainValidator EmailValidator]
           [java.util UUID]))

(defn valid-email? [^String email]
  (.isValid (EmailValidator/getInstance) email))

(def reserved-subdomains #{"www" "blog" "backend" "api" "status"})

(defn valid-subdomain? [^String subdomain]
  (boolean
    (and
      (= (str/lower-case subdomain) subdomain)              ; URLs are case-insensitive, S3 bucket names are lowercase
      (<= 3 (count subdomain) 50)                           ; conservative limit considering DB name size and S3 bucket name
      (re-find #"^[a-z][a-z0-9-]*[a-z0-9]$" subdomain)      ; limitation for DB name and S3 bucket name
      (not (contains? reserved-subdomains subdomain)) ; reserved subdomain
      (->> (config/relay-server :host)                      ; domain validation
           (str subdomain ".")
           (.isValid (DomainValidator/getInstance))
           (and (nil? (re-find #"\." subdomain)))))))

(defn valid-username? [username]
  (boolean
    (and
      (re-find #"^[a-z][a-z0-9\.\-_]*$" username)
      (<= 3 (count username) 64)
      (not (contains? #{"all" "channel" "matterbot"} username)))))

(defn valid-password? [password]
  (boolean
    (and
      (re-find #"^[a-zA-Z0-9,._+:@%/-]+$" password)
      (<= 8 (count password) 64))))

(defn valid-uuid? [uuid]
  (try
    (UUID/fromString uuid)
    true
    (catch IllegalArgumentException _e
      false)))

(s/def ::non-empty-string (s/and string? (complement str/blank?)))

(s/def ::name ::non-empty-string)
(s/def ::subdomain (s/and ::non-empty-string valid-subdomain?))
(s/def ::username (s/and ::non-empty-string valid-username?))
(s/def ::email (s/and ::non-empty-string valid-email?))
(s/def ::password (s/and ::non-empty-string valid-password?))
(s/def ::captcha-response ::non-empty-string)
(s/def ::org-id (s/and ::non-empty-string valid-uuid?))

(s/def ::signup-params
  (s/keys :req-un
          [::name ::subdomain ::username
           ::email ::password ::captcha-response]))
