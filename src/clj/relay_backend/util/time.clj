;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.util.time
  (:require [java-time :as jt]))

(defn first-day-of-month []
  (-> (jt/offset-date-time)
      (jt/adjust :first-day-of-month)
      (jt/instant)
      (jt/truncate-to :days)))

(defn first-day-of-previous-month []
  (-> (jt/offset-date-time)
      (jt/adjust :first-day-of-month)
      (jt/minus (jt/months 1))
      (jt/instant)
      (jt/truncate-to :days)))

(defn days-in-previous-month []
  (-> (jt/duration
        (first-day-of-previous-month)
        (first-day-of-month))
      (jt/as :days)))

(defn first-day-of-month? []
  (= 0 (jt/time-between (jt/instant) (first-day-of-month) :days)))
