;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.util.retry)

(defn with-retry* [retry-count wait fn-to-retry]
  (let [res (try
              (fn-to-retry)
              (catch Exception e
                (if-not (zero? retry-count)
                  ::try-again
                  (throw e))))]
    (if (= res ::try-again)
      (do
        (Thread/sleep (or wait 10))
        (recur (dec retry-count) wait fn-to-retry))
      res)))

(defmacro with-retry [{:keys [count wait]} & body]
  `(with-retry* ~count ~wait (fn [] ~@body)))
