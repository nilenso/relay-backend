;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.util.scheduler
  (:require [relay-backend.config :refer [config]]
            [clojure.tools.logging :as log]
            [mount.core :refer [defstate]])
  (:import [java.util.concurrent Executors ScheduledExecutorService TimeUnit]))

(defn start []
  (log/info "Starting the scheduler")
  (Executors/newScheduledThreadPool 1))

(defn stop [pool]
  (.shutdown ^ScheduledExecutorService pool)
  (.awaitTermination ^ScheduledExecutorService pool 1 TimeUnit/MINUTES)
  (log/info "Stopped the scheduler"))

(defstate pool
  :start (start)
  :stop (stop pool))

(defn schedule [^long delay-secs ^long period-secs ^Runnable fn-to-schedule]
  (.scheduleWithFixedDelay ^ScheduledExecutorService pool
                           fn-to-schedule
                           delay-secs
                           period-secs
                           TimeUnit/SECONDS))
