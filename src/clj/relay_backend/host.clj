;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.host
  (:require [medley.core :refer [random-uuid]]
            [relay-backend.aws :as aws]
            [relay-backend.config :as config]
            [relay-backend.db.core :as db]
            [relay-backend.db.host :as db-host]
            [relay-backend.db.org :as db-org]))

;; TODO: Serialize access to select-port
(defn- select-port [occupied-ports]
  (if (empty? occupied-ports)
    (config/amazon :port-range :min)
    (inc (apply max occupied-ports))))

(defn create-target-group [host org]
  (if-let [target-group (aws/create-target-group host (:subdomain org))]
    (db/with-transaction [tx]
      (db-org/update tx (:id org) (select-keys target-group [:target-group-arn]))
      target-group)
    nil))

(defn register-forwarding-rule [org target-arn]
  (let [priority     (rand-int 50000)
        subdomain    (:subdomain org)
        listener-arn (config/amazon :listener-arn)
        https-rule   (aws/register-forwarding-rule subdomain target-arn listener-arn priority)]
    (if https-rule
      (db/with-transaction [tx]
        (db-org/update tx (:id org) {:listener-rule-arn (:rule-arn https-rule)})
        https-rule)
      nil)))

(defn select-host-to-deploy-org [org]
  (db/with-transaction [tx]
    (let [selected-host (db-host/host-with-least-number-of-orgs tx)
          selected-port (select-port (:occupied-ports selected-host))]
      (db-org/update tx (:id org) {:host-id (:id selected-host)
                                   :port    selected-port})
      (assoc selected-host :selected-port selected-port))))

(defn setup-routing [host org]
  (let [target-group (create-target-group host org)]
    (and target-group
         (aws/register-instance-to-target-group host target-group)
         (register-forwarding-rule org (:target-group-arn target-group))
         true)))

(defn teardown-routing [org]
  (db/with-transaction [tx]
    (let [{:keys [target-group-arn listener-rule-arn] :as org}
          (db-org/find tx (select-keys org [:id]))]
      (when-not (nil? listener-rule-arn)
        (aws/delete-forwarding-rule listener-rule-arn))
      (when-not (nil? target-group-arn)
        (aws/delete-target-group target-group-arn))
      (db-org/update tx
                     (:id org)
                     (assoc org :host-id nil
                                :port nil
                                :target-group nil
                                :rule-arn nil)))))

(defn setup-existing-orgs [orgs instance-id]
  (when-let [host (db/with-transaction [tx]
                    (db-host/find tx {:instance-id instance-id}))]
    (doseq [{:keys [subdomain port]} orgs]
      (when-let [org (db/with-transaction [tx]
                       (db-org/find tx {:subdomain subdomain}))]
        (db/with-transaction [tx]
          (db-org/update tx (:id org) {:host-id (:id host)
                                       :port    port}))
        (setup-routing (assoc host :selected-port port) org)))))

(defn create-host [instance-id private-ipaddr capacity]
  (db/with-transaction [tx]
    (db-host/create tx {:id             (random-uuid)
                        :instance-id    instance-id
                        :capacity       capacity
                        :private-ipaddr private-ipaddr})))