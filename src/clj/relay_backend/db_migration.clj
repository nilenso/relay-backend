;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.db-migration
  (:require [relay-backend.config :as config]
            [clojure.tools.logging :as log]
            [ragtime.jdbc :as jdbc]
            [ragtime.repl :as ragtime]
            [ragtime.strategy :as strategy]))

(defn- migration-config []
  {:datastore  (jdbc/sql-database {:connection-uri (config/db-jdbc-uri)})
   :migrations (jdbc/load-resources "migrations")
   :strategy   (if (or (config/dev-env?) (config/test-env?)) strategy/apply-new strategy/raise-error)
   :reporter   (fn [_ op id]
                 (case op
                   :up (log/info "Applying migration" id)
                   :down (log/info "Rolling back migration" id)))})

(defn migrate []
  (ragtime/migrate (migration-config))
  (log/info "Ran all migrations"))

(defn rollback [args]
  (let [amount-or-id (or (:to args) (Integer/parseInt (or (:last args) "1")))]
    (if (and amount-or-id (not= 1 amount-or-id))
      (ragtime/rollback (update (migration-config) :migrations vec) amount-or-id)
      (ragtime/rollback (update (migration-config) :migrations #(vector (last %)))))))