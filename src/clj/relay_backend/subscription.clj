;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.subscription
  (:require [cheshire.core :as json]
            [clj-http.client :as http]
            [clojure.tools.logging :as log]
            [java-time :as jt]
            [relay-backend.config :as config]
            [relay-backend.db.core :as db]
            [relay-backend.db.org :as db-org]
            [relay-backend.db.payment :as db-payment]
            [relay-backend.db.subscription :as db-subs]
            [relay-backend.db.charge-attempt :as db-ca]
            [relay-backend.fastspring :as fastspring]
            [relay-backend.instance :as instance]
            [relay-backend.util.scheduler :as scheduler]
            [relay-backend.util.time :as ut]))

(defn get-active-user-count [{:keys [subdomain] :as _org}]
  (let [oauth-token (instance/login-bot subdomain)
        resp        (http/get (instance/api-url subdomain "analytics/old")
                              {:oauth-token           oauth-token
                               :query-params          {"name" "standard"}
                               :throw-entire-message? true})]
    (if (http/success? resp)
      (let [analytics (-> resp (:body) (json/decode true))]
        (->> analytics
             (filter #(= (:name %) "monthly_active_users"))
             (first)
             (:value)))
      (log/errorf "Could not get active user count. Error: %s" (:body resp)))))

(defn update-and-charge-subscription [subs-id {:keys [amount] :as charge-attempt}]
  (let [ca     (db/with-transaction [tx] (db-ca/create tx charge-attempt))
        result (and (fastspring/update-subscription subs-id amount)
                    (fastspring/charge-subscription subs-id))]
    (when-not result
      (db/with-transaction [tx]
        (db-ca/update tx (assoc ca :status (:request-failed db-ca/charge-attempt-status)
                                   :failure-reason "Update or charge request failed"))))
    result))

(defn floor-currency [amount]
  (/ (Math/floor (* 100 amount)) 100))

(defn calc-charge-amount [active-user-count days-created]
  (let [monthly-active-user-price (config/subscription :monthly-active-user-price)
        days-in-month             (ut/days-in-previous-month)
        prorata-factor            (if (> days-created days-in-month)
                                    1
                                    (/ days-created days-in-month))]
    (floor-currency (* prorata-factor active-user-count monthly-active-user-price))))

(defn org-with-subs-payment [tx org-id]
  (if-let [org (db-org/find tx {:id org-id})]
    (if-let [subs (db-subs/find tx {:org-id org-id :status "active"})]
      (assoc org :subscription subs
                 :payment (db-payment/find tx {:org-id         org-id
                                               :subs-id        (:subs-id subs)
                                               :completed      true
                                               :created-after  (ut/first-day-of-month)
                                               :created-before (jt/instant)})
                 :charge-attempts (db-ca/list tx {:org-id  org-id
                                                  :subs-id (:id subs)
                                                  :status  (:attempted db-ca/charge-attempt-status)})))))

(defn- do-charge-subscription [{:keys [subscription id] :as _org} active-user-count]
  (let [today          (ut/first-day-of-month)
        created-at     (:created-at subscription)
        days-created   (jt/time-between created-at today :days)
        amount         (calc-charge-amount active-user-count days-created)
        charge-attempt {:org-id            id
                        :status            (:attempted db-ca/charge-attempt-status)
                        :subs-id           (:id subscription)
                        :active-user-count active-user-count
                        :amount            amount
                        :period-start      created-at
                        :period-end        today}]
    (when (update-and-charge-subscription (:subs-id subscription) charge-attempt)
      (log/infof "Successfully charged subscription for org; org-id: %s" id))))

(defn charge-subscription [org-id]
  (log/infof "Attempting to charge subscription for org; org-id: %s" org-id)
  (let [{:keys [payment charge-attempts] :as org} (db/with-transaction [tx]
                                                    (org-with-subs-payment tx org-id))]
    (cond
      (not org)
      (log/infof "Subscription not found; org-id: %s" org-id)

      (not (nil? payment))
      (log/infof "A payment already present for this month. org-id: %s, payment-id: %s" org-id (:id payment))

      (not (empty? charge-attempts))
      (log/infof "A charge attempt is already present. org-id: %s, charge-attempt-id: %s" org-id (:id (first charge-attempts)))

      :else (let [active-user-count (get-active-user-count org)]
              (if (not= 0 active-user-count)
                (do-charge-subscription org active-user-count)
                (log/infof "Active monthly count is nil or zero. Not charging org; org-id: %s" org-id))))))

(defn charge-subscriptions []
  (when (ut/first-day-of-month?)
    (log/infof "Starting scheduled charging of subscriptions")
    (doseq [{:keys [id] :as _org} (db/with-transaction [tx]
                                    (db-org/list tx {:status (:running db-org/org-status)}))]
      (try
        (charge-subscription id)
        (catch Exception e
          (log/errorf e "Error in charging org. org-id: %s" id))))
    (log/infof "Finished scheduled charging of subscriptions")))

(defn schedule-charging []
  (let [period (-> (config/subscription :period-hours)
                   (jt/duration :hours)
                   (jt/as :seconds))]
    (scheduler/schedule 1 period charge-subscriptions)))
