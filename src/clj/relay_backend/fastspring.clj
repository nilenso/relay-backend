;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.fastspring
  (:require [camel-snake-kebab.core :as csk]
            [cheshire.core :as json]
            [clj-http.client :as http]
            [clojure.set :as set]
            [clojure.string :as str]
            [clojure.tools.logging :as log]
            [java-time :as jt]
            [medley.core :refer [uuid]]
            [relay-backend.config :as config]
            [relay-backend.db.charge-attempt :as charge-attempt :refer [charge-attempt-status]]
            [relay-backend.db.core :as db]
            [relay-backend.db.payment :as payment]
            [relay-backend.db.subscription :as subscription]
            [relay-backend.db.webhook-event :as webhook-event]
            [relay-backend.notifications :as notify]
            [relay-backend.util.map :as umap]))

;; WORKFLOW
;;
;; - immediately after subscription from UI:
;;   <- subscription.activated
;;   <- order.completed/order.failed
;;
;; - after a month:
;;   -> subscription.update
;;   -> subscription.charge
;;   <- subscription.charge.completed / subscription.charge.failed

(defn- api-url [path]
  (str (config/fastspring :host) path))

(defn- post [path body]
  (let [{:keys [username password]} (config/fastspring :api-credentials)
        resp (http/post (api-url path)
                        {:basic-auth       [username password]
                         :body             (json/encode body)
                         :throw-exceptions false})]
    (if (http/success? resp)
      (-> resp (:body) (json/decode true))
      (log/errorf "Error in post request %s; Error: %s" path (:body resp)))))

(defn update-subscription [subscription-id amount]
  (when-let [subscription (-> (post "/subscriptions" {:subscriptions
                                                      [{:subscription subscription-id
                                                        :pricing      {:price {"USD" amount}}}]})
                              :subscriptions
                              (first))]
    (if (= "success" (:result subscription))
      (do (notify/info "Updated subscription" :subscription-id subscription-id :amount amount)
          true)
      (do (log/errorf "Could not update subscription: %s" (:error subscription))
          false))))

(defn charge-subscription [subscription-id]
  (when-let [subscription (-> (post "/subscriptions/charge"
                                    {:subscriptions [{:subscription subscription-id}]})
                              :subscriptions
                              (first))]
    (if (= "success" (:result subscription))
      (do (notify/info "Charged subscription" :subscription-id subscription-id)
          true)
      (do (log/errorf "Could not charge subscription: %s" (:error subscription))
          false))))

(defmulti handle-event
  "Handle an event from fastspring"
  (fn [event] (:type event)))

(defmethod handle-event :default
  [_event]
  nil)

(defmethod handle-event "subscription.activated"
  [{:keys [data] :as event}]
  (let [{:keys [id currency state tags]} data]
    (if-let [org-id (some-> tags (:org-id) (uuid))]
      (do
        (db/with-transaction [tx]
          (subscription/create tx {:org-id   org-id
                                   :subs-id  id
                                   :status   state
                                   :currency currency}))
        (notify/info "Subscription Activated" :org-id org-id))
      (log/errorf "No tag found in the webhook event: type:%s id:%s" (:type event) (:id event)))))

(defn fastspring->domain [order]
  (set/rename-keys order {:order                    :order-id
                          :currency                 :charge-currency
                          :total                    :charge-total
                          :tax                      :charge-tax
                          :total-in-payout-currency :payout-total
                          :tax-in-payout-currency   :payout-tax}))

(defn- handle-charge-event [tx order subscription]
  (if-let [org-id (some-> subscription :tags :org-id (uuid))]
    (let [created-at (-> order :changed (jt/instant))
          payment    (-> order
                         (fastspring->domain)
                         (assoc :org-id org-id
                                :subs-id (:id subscription)
                                :created-at created-at))]
      (payment/create tx payment))))

(defn- handle-order-event [event]
  (let [order        (:data event)
        subscription (-> event :data :items first :subscription)]
    (when-not (db/with-transaction [tx] (handle-charge-event tx order subscription))
      (log/errorf "Unable to save payment for event: type:%s id:%s" (:type event) (:id event)))))

(defn- update-charge-attempt-status [tx org-id subs-id changes]
  (let [charge-attempt (charge-attempt/charge-attempts-for-org-subs tx org-id subs-id)]
    (charge-attempt/update tx (merge charge-attempt changes))))

(defmethod handle-event "order.completed"
  [event]
  (handle-order-event event))

(defmethod handle-event "order.failed"
  [event]
  (handle-order-event event))

(defmethod handle-event "subscription.charge.completed"
  [{:keys [data] :as event}]
  (let [order        (:order data)
        subscription (:subscription data)
        org-id       (some-> subscription :tags :org-id (uuid))
        subs-id      (:id subscription)]
    (db/with-transaction [tx]
      (if-let [{:keys [id] :as _payment} (handle-charge-event tx order subscription)]
        (update-charge-attempt-status tx org-id subs-id
                                      {:status     (:success charge-attempt-status)
                                       :payment-id id})
        (log/errorf "Unable to save payment for event: type:%s id:%s" (:type event) (:id event))))))

(defmethod handle-event "subscription.charge.failed"
  [{:keys [data] :as event}]
  (if-let [org-id (some-> data :subscription :tags :org-id (uuid))]
    (let [subs-id (-> data :subscription :id)
          reason  (-> data :reason)]
      (db/with-transaction [tx]
        (update-charge-attempt-status tx org-id subs-id
                                      {:status         (:failed charge-attempt-status)
                                       :failure-reason reason})))
    (log/errorf "No tag found in the webhook event: type:%s id:%s" (:type event) (:id event))))

(defn- do-handle-event [{:keys [id] :as event}]
  (log/debug "Received webhook event from fastspring" event)
  (db/with-transaction [tx]
    (webhook-event/create tx "fastspring" event))
  (try
    (handle-event (umap/nested-map-keys csk/->kebab-case-keyword event))
    {:id id :success true}
    (catch Exception e
      (log/error e "Error in handling webhook event from fastspring")
      {:id id :success false})))

(defn handle-events [{:keys [params] :as _req}]
  (let [results     (doall (for [event (:events params)] (do-handle-event event)))
        success-ids (->> results (filter :success) (map :id))]
    {:status  202
     :headers {"Content-Type" "text/plain"}
     :body    (str/join "\n" success-ids)}))
