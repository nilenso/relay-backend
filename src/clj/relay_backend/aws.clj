;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(ns relay-backend.aws
  (:require [amazonica.aws.ec2 :as ec2]
            [amazonica.aws.elasticloadbalancingv2 :as elb]
            [relay-backend.config :as config]
            [clojure.tools.logging :as log]))

(defn create-target-group [{:keys [selected-port]} subdomain]
  (let [target-group-name (format "%s-%s" (first (name (config/env))) subdomain)]
    (log/infof "Creating target group for subdomain: %s, selected-port: %s" subdomain selected-port)
    (-> (config/amazon :target-groups)
        (merge {:name target-group-name :port selected-port})
        elb/create-target-group
        :target-groups
        first)))

(defn register-instance-to-target-group [{:keys [instance-id selected-port]} target-group]
  (let [target-group-arn (:target-group-arn target-group)
        target           {:id instance-id :port selected-port}
        request          {:target-group-arn target-group-arn
                          :targets          [target]}]
    (log/infof "Registering instance to target group instance-id: %s, selected-port: %s, target-group: %s"
               instance-id
               selected-port
               target-group)
    (elb/register-targets request)))

(defn register-forwarding-rule [subdomain target-group-arn listener-arn priority]
  (let [action         {:target-group-arn target-group-arn :type "forward"}
        hostname       (format "%s.%s" subdomain (config/relay-server :host))
        rule-condition {:field "host-header" :values [hostname]}
        request        {:actions      [action]
                        :conditions   [rule-condition]
                        :listener-arn listener-arn
                        :priority     priority}]
    (log/infof "Registering forwarding rule for subdomain: %s, listener-arn: %s" subdomain listener-arn)
    (-> request
        elb/create-rule
        :rules
        first)))

(defn delete-target-group [target-group-arn]
  (log/infof "Deleting target group: %s" target-group-arn)
  (elb/delete-target-group {:target-group-arn target-group-arn}))

(defn delete-forwarding-rule [rule-arn]
  (log/infof "Deleting listener rule: %s" rule-arn)
  (elb/delete-rule {:rule-arn (str rule-arn)}))