;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.dashboard.view
  (:require [relay-backend.views.common :as c]
            [re-frame.core :refer [subscribe dispatch]]
            [clojure.string :as str]))

(defn trial-message [info settings]
  [:p "Your organisation "
   [:a {:href   (str "https://" (get-in info [:org :name]) "." (:domain settings))
        :target "_blank"}
    (get-in info [:org :name])]
   [:span " has "]
   [:strong (get-in info [:org :days-left])]
   [:span " days left in the free trial."]])

(defn subscribed-message [info settings]
  [:p "Your organisation "
   [:a {:href   (str "https://" (get-in info [:org :name]) "." (:domain settings))
        :target "_blank"}
    (get-in info [:org :name])]
   [:span " has an active subscription."]])

(defn panel []
  (let [info        (subscribe [:dashboard])
        settings    (subscribe [:settings])
        sysadmin?   (str/includes? (get-in @info [:user :roles]) "system_admin")
        subscribed? (some? (get-in @info [:org :subscription]))]
    [:div.login-container
     [c/header-without-links]
     [:div.login
      [:h1.page-heading "Dashboard"]
      [:div.form-login
       [:div.form-header
        [:h2.section-heading (str "Hello, " (get-in @info [:user :username]) "!")]
        [:hr.section-hr.center]]
       [:div.form-body
        [:div.dashboard
         (if subscribed?
           [subscribed-message @info @settings]
           [trial-message @info @settings])
         (when-not (or subscribed? sysadmin?)
           [:p "Contact your organisation's administrator to subscribe to Relay."])]]
       [:div.form-footer {:style {:justify-content "center"}}
        (when (and sysadmin? (not subscribed?))
          [:button {:data-fsc-item-path       "relay-monthly-subscription"
                    :data-fsc-item-path-value "relay-monthly-subscription"
                    :data-fsc-action          "Checkout"
                    :class                    "button primary"}
           [:span "Subscribe"]])
        [:a.button.empty {:href "#" :on-click #(dispatch [:logout])}
         "Logout"]]]]]))
