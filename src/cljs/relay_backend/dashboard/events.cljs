;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.dashboard.events
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [re-frame.core :refer [reg-event-fx reg-event-db reg-fx dispatch]]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]))

(defn logout [_  _]
  (go
    (let [resp (<! (http/delete "/api/v1/users/me"))]
      (when (:success resp)
        (dispatch [:dashboard :user nil])
        (dispatch [:dashboard :org nil])
        (dispatch [:redirect "/"]))))
  {})

(defn init-fastspring [org-id]
  (let [tags {:org-id org-id}]
    (aset js/window "fscSession"
          (clj->js
            {:reset    true
             :products [{:path     "relay-monthly-subscription"
                         :quantity 1}]
             :tags     tags}))
    (.push (.-builder js/fastspring) (.-fscSession js/window))))

(defn load-dashboard [{:keys [db]} _]
  (go
    (let [user-resp (<! (http/get "/api/v1/users/me"))]
      (when (:success user-resp)
        (let [{:keys [org-id] :as user} (:body user-resp)
              org-resp (<! (http/get (str "/api/v1/orgs/" org-id)))]
          (when (:success org-resp)
            (init-fastspring org-id)
            (dispatch [:dashboard :user user])
            (dispatch [:dashboard :org (:body org-resp)]))))
      (dispatch [:dashboard :waiting? false])))
  {:dispatch [:set-active-panel :dashboard]
   :db       (assoc-in db [:dashboard :waiting?] true)})

(defn ^:export on-popup-close [event]
  (when (some? event)
    (dispatch [:dashboard :org :subscription
               (js->clj event :keywordize-keys true)])))

(defn init []
  (reg-event-fx :load-dashboard load-dashboard)
  (reg-event-fx :logout logout))
