;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.events
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [re-frame.core :refer [reg-event-fx reg-event-db reg-fx dispatch]]
            [relay-backend.db :as db]
            [cljs.core.async :refer [<!]]))

(defn set-active-panel [db [_ active-panel & [args]]]
  (assoc db
    :active-panel
    {:name active-panel
     :args args}))

(defn assoc-into-db [k]
  (fn [db [_ & args]]
    (let [ks    (butlast args)
          value (last args)]
      (assoc-in db (cons k ks) value))))

(defn init []
  (reg-event-db :initialize-db (fn [_ _] db/default-db))
  (reg-event-db :set-active-panel set-active-panel)
  (reg-event-db :user-input (assoc-into-db :user-input))
  (reg-event-db :dashboard (assoc-into-db :dashboard)))
