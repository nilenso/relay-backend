;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.login.view
  (:require [relay-backend.views.common :as c]
            [re-frame.core :refer [subscribe dispatch]]))

(defn panel []
  (let [form-data (subscribe [:user-input :login-form])]
    [:div.login-container
     [c/header-without-links]
     [:div.login
      [:h1.page-heading "login"]
      [:form.form-login
       [:div.form-header
        [:h2.section-heading "Have a relay instance? Login below."]
        [:hr.section-hr.center]]
       [:div.form-body
        [:div.input-subdomain
         [:div.input-container
          [:input.form-input
           {:id           "login-subdomain"
            :name         "subdomain"
            :placeholder  "Enter your subdomain"
            :required     "required"
            :type         "text"
            :class        (if (get-in @form-data [:subdomain :error]) "error" "")
            :value        (get-in @form-data [:subdomain :value])
            :on-change    (fn [e]
                            (dispatch [:user-input :login-form :subdomain :value (c/tvalue e)]))
            :on-key-up    (fn [e]
                            (when (= "Enter" (.-key e))
                              (.preventDefault e)
                              (dispatch [:submit-login-form])))
            :on-key-press (fn [e]
                            (when (= "Enter" (.-key e))
                              (.preventDefault e)))
            :autoFocus    "autofocus"}]]]
        [:p.input-error (get-in @form-data [:subdomain :error])]]
       [:div.form-footer.left-right
        [:div.left]
        [:div.right
         [:a.button.empty {:href "/"} "Back Home"]
         [:a.button.primary {:href "#" :on-click #(dispatch [:submit-login-form])}
          "Login"]]]]]]))
