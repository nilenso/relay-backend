;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.login.events
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [re-frame.core :refer [reg-event-fx reg-event-db reg-fx dispatch]]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]
            [clojure.string :as s]))

(defn subdomain-available? [subdomain]
  (http/get "/api/v1/subdomain-availability"
            {:query-params {:subdomain subdomain}}))

(defn submit-form [{:keys [db]} _]
  (let [params (->> (get-in db [:user-input :login-form])
                    (map (fn [[k v]] [k (:value v)]))
                    (into {}))
        subdomain (:subdomain params)]
    (if subdomain
      (do
        (go
          (let [response (<! (subdomain-available? subdomain))]
            (if (and (:success response)
                     (get-in response [:body :available]))
              (dispatch [:user-input :login-form :subdomain :error "Invalid Subdomain"])
              (dispatch [:redirect (str "/oauth/" subdomain)]))))
        {})
      {:db (assoc-in db [:user-input :login-form :subdomain :error] "Subdomain required")})))

(defn init []
  (reg-event-fx :submit-login-form submit-form))
