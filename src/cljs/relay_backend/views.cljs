;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.views
  (:require [re-frame.core :refer [subscribe dispatch]]
            [relay-backend.views.common :as c]
            [relay-backend.dashboard.view :as dashboard]
            [relay-backend.login.view :as login]
            [relay-backend.signup.view :as signup]))

(defn when-logged-in [panel]
  (let [user (subscribe [:dashboard :user])]
    (if (some? @user)
      [panel]
      [login/panel])))

(defn not-found []
  [c/app-container
   "Wups"
   nil
   [:p "This page was not found"]
   [:a.button.empty {:href "/"}
    "Back Home"]])

(defn- panels [panel-name]
  (case panel-name
    :home-page [:p]
    :dashboard (when-logged-in dashboard/panel)
    :signup [signup/signup]
    :login [login/panel]
    :not-found [not-found]
    [:div]))

(defn main-panel []
  (let [active-panel (subscribe [:active-panel])]
    [panels (:name @active-panel)]))
