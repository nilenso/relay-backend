;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import goog.History)
  (:require [secretary.core :as secretary]
            [re-frame.core :refer [dispatch reg-event-fx]]
            [pushy.core :as pushy]))

(defonce history
    (pushy/pushy secretary/dispatch!
                 (fn [x] (when (secretary/locate-route x) x))))

(defn navigate [_ [_ path]]
  (pushy/set-token! history path))

(defn redirect [_ [_ path]]
  (set! js/window.location.href path)
  {})

(defn init []

  (js/window.addEventListener
   "DOMContentLoaded"
   (fn [e]
     (secretary/dispatch! (pushy/get-token history))))

  (defroute "/" []
    (dispatch [:redirect "/"]))

  (defroute "/community" []
    (dispatch [:redirect "/community"]))

  (defroute "/eula" []
    (dispatch [:redirect "/eula"]))

  (defroute "/privacy" []
    (dispatch [:redirect "/privacy"]))

  (defroute #"/login.*" [path args]
    (when-let [err (some-> args :query-params :error)]
      (dispatch [:user-input :login-form :subdomain :error (js/decodeURIComponent err)]))
    (dispatch [:load-dashboard]))

  (defroute "/dashboard" []
    (dispatch [:load-dashboard]))

  (defroute "*" []
    (dispatch [:set-active-panel :not-found]))

  (reg-event-fx :navigate navigate)
  (reg-event-fx :redirect redirect)

  (pushy/start! history))
