;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.signup.view
  (:require [relay-backend.views.common :as c]
            [re-frame.core :refer [subscribe dispatch]]
            [reagent.core :as reagent]))

(defn input-error [form-data input-name]
  [:p.input-error (or (get-in form-data [(keyword input-name) :error]) " ")])

(defn signup-form-input [& _]
  (fn [form-data input-name input-placeholder input-type args]
    (let [error? (not-empty (get-in form-data [(keyword input-name) :error]))]
      [:div {:class (str "input-" input-name)}
       [:div.input-container
        [:input (merge {:id          input-name
                        :name        input-name
                        :placeholder input-placeholder
                        :class       (str "form-input " (if error? "error" ""))
                        :required    "required"
                        :type        input-type
                        :value       (get-in form-data [(keyword input-name) :value])
                        :on-change   (fn [e]
                                       (dispatch [:user-input :signup-form (keyword input-name) :value (c/tvalue e)]))
                        :on-key-up   (fn [e]
                                       (when (= "Enter" (.-key e))
                                         (.preventDefault e)
                                         (dispatch [:goto-next-step])))
                        :on-key-press (fn [e]
                                        (when (= "Enter" (.-key e))
                                         (.preventDefault e)))}
                       args)]]
       [input-error form-data input-name]])))

(defn render-recaptcha [container-id]
  (.render js/grecaptcha
           container-id
           (clj->js {:sitekey "6LfnWD4UAAAAAIOrjeb6pexIOfDOcj08Sfb-S2-U"
                     :theme "light"}))
  nil)

(defn step-1 [form-data]
  [:form.form-signup
   [:div.form-header
    [:h2.section-heading "tell us where you work"]
    [:hr.section-hr.center]]
   [:div.form-body
    [signup-form-input form-data "name" "Enter your organisation name" "text" {:autoFocus "autoFocus"}]
    [signup-form-input form-data "email" "Enter your work email" "text" nil]]
   [:div.form-footer.left-right
    [:div.left
     [:a.button.empty {:on-click #(dispatch [:navigate "/login"])} "Login"]]
    [:div.right
     [:a.button.empty {:href "/"} "Back Home"]
     [:a.button.primary {:href "#" :on-click #(dispatch [:goto-next-step])}
      "Next Step"]]]])

(defn step-2 [form-data]
  [:form.form-signup
   [:div.form-header
    [:h2.section-heading "create your relay account"]
    [:hr.section-hr.center]]
   [:div.form-body
    [signup-form-input form-data "username" "Pick a username (eg: bill, steve_jobs)" "text" {:autoFocus "autoFocus"}]
    [signup-form-input form-data "password" "Enter your password" "password"]]
   [:div.form-footer
    [:a.button.empty {:href "#" :on-click #(dispatch [:goto-previous-step])}
     "Previous Step"]
    [:a.button.primary {:href "#" :on-click #(dispatch [:goto-next-step])}
     "Final Step"]]])

(defn step-3 [form-data]
  (reagent/create-class
   {:component-did-mount
    #(render-recaptcha "recaptcha-container")

    :reagent-render
    (fn [form-data]
      [:form.form-signup
       [:div.form-header
        [:h2.section-heading "choose your company url"]
        [:hr.section-hr.center]]
       [:div.form-body
        [signup-form-input form-data "subdomain" "Choose your subdomain" "text" {:autoFocus "autoFocus"}]
        [:div {:id "recaptcha-container" :class "form-input"}]
        [input-error form-data "captcha"]
        [:p.tos "By signing up, you agree to our "
           [:a {:href "/eula" :target "_blank"} "EULA"]
           " and "
            [:a {:href "/privacy" :target "_blank"} "privacy policy"]
            "."]]
       [:div.form-footer
        [:a.button.empty {:href "#" :on-click #(dispatch [:goto-previous-step])}
         "Previous Step"]
        [:a.button.primary {:href "#" :on-click #(dispatch [:goto-next-step])}
         "Start Trial"]]])}))

(defn form []
  (let [form-data (subscribe [:user-input :signup-form])]
    (fn []
      (case (:step @form-data)
        1 [step-1 @form-data]
        2 [step-2 @form-data]
        3 [step-3 @form-data]
        [step-3 @form-data]))))

(defn status-panel [heading icon-class message & [footer]]
  [:form.form-signup
   [:div.form-header
    [:h2.section-heading heading]
    [:hr.section-hr.center]]
   [:div.form-body.status
    [:i {:class icon-class
         :style {:font-size "48px"}}]
    [:div.status-message message]]
   (when footer footer)])

(defn waiting []
  [status-panel
   "Spinning up your instance"
   "fa fa-spinner fa-spin waiting-icon"
   [:p "This will only be a moment."]])

(defn success []
  [status-panel
   "Your relay instance is now active"
   "fas fa-check-square success-icon"
   [:p "Please check your inbox to verify your email address."]
   [:div.form-footer
    [:a.button.empty {:href "/"} "Back Home"]]])

(defn failure []
  (let [settings (subscribe [:settings])]
    [status-panel
     "Unable to spin up the instance"
     "fas fa-exclamation-circle failure-icon"
     [:p "Please contact us at "
      [:a {:href "mailto:team@relay-chat.com"} (:feedback-email @settings)]
      "."]
     [:div.form-footer
      [:a.button.empty {:href "/"} "Back Home"]]]))

(defn step-item [this-step step-name current-step]
  (let [active-class (if (= this-step current-step) "active" "")]
    (if (> current-step this-step)
      [:li.step
       [:p.number.done {:class active-class} [:img.icon {:src "/icons/check.svg"}]]
       [:p.step-name step-name]]
      [:li.step
       [:p.number {:class active-class} (str this-step)]
       [:p.step-name step-name]])))

(defn signup []
  (let [step (subscribe [:user-input :signup-form :step])
        status (subscribe [:user-input :signup-form :status])]
    [:div.signup-container
     [c/header-without-links]
     [:div.signup
      [:h1.page-heading "Sign up"]
      [:p.byline "for a 30 day free trial"]
      [:ul.steps
       [step-item 1 "Enter org name" @step]
       [step-item 2 "Create account" @step]
       [step-item 3 "Claim Relay URL" @step]]
      [:hr.step-connector]
      (case @status
        :filling [form]
        :waiting [waiting]
        :success [success]
        :failure [failure])]]))
