;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.signup.events
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [re-frame.core :refer [reg-event-fx reg-event-db reg-fx dispatch]]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]
            [clojure.string :as s]))

(defn validate-subdomain [{:keys [db]} _]
  (let [subdomain (get-in db [:user-input :signup-form :subdomain :value])]
    (go
      (let [response (<! (http/get "/api/v1/subdomain-availability"
                                   {:query-params {:subdomain subdomain}}))]
        (if (:success response)
          (if (get-in response [:body :available])
            (do (dispatch [:user-input :signup-form :subdomain :error nil])
                (dispatch [:submit-signup-form]))
            (do (dispatch [:user-input :signup-form :subdomain :error "Subdomain Unavailable"])
                (dispatch [:goto-previous-step])))
          (dispatch [:user-input :signup-form :subdomain :error "Error connecting to server"]))))
    {:dispatch [:user-input :signup-form :subdomain :error "Checking if subdomain is available"]}))

(defn invalid-email? [email]
  (->> email
       (re-find #"(?i)^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$")
       (nil?)))

(defn input-options [& mono-strings]
  [:span
   (for [s mono-strings]
     ^{:key (str (random-uuid))}
     [:span.mono s])])

(def reserved-subdomains #{"www" "blog" "backend" "api" "status"})

(def validations
  {1 {:name  [{:check s/blank? :error "Organisation name is required"}]
      :email [{:check s/blank? :error "Work email is required"}
              {:check invalid-email? :error "Email is not valid"}]}

   2 {:username [{:check s/blank? :error "Username is required"}

                 {:check #(not (re-find #"^[a-z][a-z0-9\.\-_]*$" %))
                  :error [:span "Username must start with "
                          [input-options "a-z"]
                          " and only have "
                          [input-options "a-z" "0-9" "-" "_" "."]]}

                 {:check #(not (<= 3 (count %) 64))
                  :error "Username length must be between 3 and 64 characters"}

                 {:check #(contains? #{"all" "channel" "matterbot"} %)
                  :error "This is a restricted username. Please choose another."}]

      :password [{:check s/blank? :error "Password is required"}

                 {:check #(not (re-find #"^[a-zA-Z0-9,._+:@%/-]+$" %))
                  :error [:span "Password can only have "
                          [input-options "A-Z" "a-z" "0-9" "@" "%" "-" "_" "+" ":" "," "." "/"]]}

                 {:check #(not (<= 8 (count %) 64))
                  :error "Password length must be at between 8 and 64 characters."}]}

   3 {:subdomain [{:check s/blank? :error "Subdomain is required"}

                  {:check #(not= (s/lower-case %) %) :error "Subdomain must be all lowercase"}

                  {:check #(not (re-find #"^[a-z][a-z0-9-]*[a-z0-9]$" %))
                   :error [:span "Subdomain must start with "
                           [input-options "a-z"]
                           ", only have "
                           [input-options "a-z" "0-9" "-"]
                           " and not end with a "
                           [input-options "-"]]}

                  {:check #(not (<= 3 (count %) 50))
                   :error "Subdomain length must be between 3 and 50 characters"}

                  {:check #(contains? reserved-subdomains %)
                   :error "This is a reserved subdomain. Please choose another."}]
      :captcha   [{:check (fn [_] (s/blank? (.getResponse js/grecaptcha))) :error "Please check the CAPTCHA"}]}})

(def params-in-step
  (->> validations
       (map (fn [[step vs]] [step (set (keys vs))]))
       (into {})))

(defn validate-step [db step]
  (reduce
    (fn [db [param vs]]
      (let [error-path [:user-input :signup-form param :error]
            value-path [:user-input :signup-form param :value]]
        (if-let [error (some (fn [{:keys [check error]}]
                               (if (check (get-in db value-path)) error nil))
                             vs)]
          (assoc-in db error-path error)
          (assoc-in db error-path nil))))
    db
    (validations step)))

(defn no-errors? [db]
  (->> (get-in db [:user-input :signup-form])
       vals
       (filter map?)
       (keep :error)
       empty?))

(defn goto-next-step [{:keys [db]} _]
  (let [step         (get-in db [:user-input :signup-form :step])
        validated-db (validate-step db step)]
    (if (no-errors? validated-db)
      (if (>= step 3)
        {:dispatch [:validate-subdomain]
         :db       (update-in validated-db [:user-input :signup-form :step] inc)}
        {:db (update-in validated-db [:user-input :signup-form :step] inc)})
      {:db validated-db})))

(defn goto-previous-step [db _]
  (let [step   (get-in db [:user-input :signup-form :step])
        params (params-in-step step)]
    (-> (reduce (fn [db param]
                  (assoc-in db [:user-input :signup-form param :error] nil))
                db
                params)
        (update-in [:user-input :signup-form :step] dec))))

(defn poll-org-status [_ [_ org-id]]
  (go
    (let [response (<! (http/get (str "/api/v1/orgs/" org-id)))]
      (if (:success response)
        (case (get-in response [:body :status])
          "running" (dispatch [:user-input :signup-form :status :success])
          "failed" (dispatch [:user-input :signup-form :status :failure])
          (js/setTimeout #(dispatch [:poll-org-status org-id]) 1000))
        (js/setTimeout #(dispatch [:poll-org-status org-id]) 1000))))
  {})

(defn submit-form [{:keys [db]} _]
  (let [form-data        (->> (get-in db [:user-input :signup-form])
                              (map (fn [[k v]] [k (:value v)]))
                              (into {}))
        captcha-response (.getResponse js/grecaptcha)
        params           (assoc form-data
                           :captcha-response captcha-response)]
    (go
      (let [response (<! (http/post "/api/v1/orgs" {:json-params params}))]
        (if (:success response)
          (do
            (dispatch [:user-input :signup-form :status :waiting])
            (dispatch [:poll-org-status (-> response :body :id)]))
          (do
            (js/console.log (str "Error in signing up: " response))
            (dispatch [:user-input :signup-form :status :failure])))))
    {:db (assoc-in db [:user-input :signup-form :status] :waiting)}))

(defn load-signup [{:keys [db]} [_ query-params]]
  {:db       (assoc-in db [:user-input :signup-form :email :value] (:email query-params))
   :dispatch [:set-active-panel :signup]})

(defn init []
  (reg-event-fx :goto-next-step goto-next-step)
  (reg-event-db :goto-previous-step goto-previous-step)

  (reg-event-fx :load-signup load-signup)
  (reg-event-fx :validate-subdomain validate-subdomain)
  (reg-event-fx :submit-signup-form submit-form)
  (reg-event-fx :poll-org-status poll-org-status))
