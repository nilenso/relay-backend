;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.views.common
  (:require [re-frame.core :refer [dispatch]]))

(defn tvalue [obj]
  (-> obj .-target .-value))

(defn header-without-links []
  [:header {:class "header"}
    [:div.logo-and-header-links.only-logo
      [:a {:href "/", :id "logo"} "relay."]]])

(defn icon [icon]
  [:img.icon {:src (str "icons/" icon)}])

(defn app-container [page-heading section-heading body footer]
  [:div.login-container.app-container
     [header-without-links]
     [:div.login
      [:h1.page-heading page-heading]
      [:div.form-login
       [:div.form-header
        (when section-heading
          [:h2.section-heading section-heading])
        [:hr.section-hr.center]]
       [:div.form-body
        body]
       [:div.form-footer
        footer]]]])
