;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [relay-backend.events :as events]
            [relay-backend.dashboard.events :as dashboard-events]
            [relay-backend.login.events :as login-events]
            [relay-backend.signup.events :as signup-events]
            [relay-backend.routes :as routes]
            [relay-backend.subs :as subscriptions]
            [relay-backend.views :as views]
            [relay-backend.config :as config]))

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (events/init)
  (login-events/init)
  (signup-events/init)
  (dashboard-events/init)
  (routes/init)
  (subscriptions/init)
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (mount-root))
