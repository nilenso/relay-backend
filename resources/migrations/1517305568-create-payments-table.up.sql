CREATE TABLE payments (
    id UUID NOT NULL PRIMARY KEY,
    order_id VARCHAR(100) NOT NULL,
    org_id UUID NOT NULL REFERENCES orgs(id),
    subs_id VARCHAR(100) NOT NULL,
    charge_total NUMERIC(12,2) NOT NULL,
    charge_tax NUMERIC(12,2) NOT NULL,
    charge_currency VARCHAR(10) NOT NULL,
    payout_total NUMERIC(12,2) NOT NULL,
    payout_tax NUMERIC(12,2) NOT NULL,
    payout_currency VARCHAR(10) NOT NULL,
    completed BOOLEAN NOT NULL,
    invoice_url VARCHAR(2048),
    created_at timestamp without time zone NOT NULL
);

CREATE INDEX IF NOT EXISTS payments_org_id_subs_id_key ON payments(org_id, subs_id);