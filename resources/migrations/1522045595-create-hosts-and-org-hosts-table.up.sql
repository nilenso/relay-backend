CREATE TABLE hosts (
    id UUID NOT NULL PRIMARY KEY,
    instance_id VARCHAR(300) NOT NULL UNIQUE,
    private_ipaddr VARCHAR(300) NOT NULL UNIQUE,
    capacity INTEGER NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);

ALTER TABLE orgs
    ADD COLUMN host_id UUID REFERENCES hosts(id),
    ADD COLUMN port INTEGER,
    ADD COLUMN target_group_arn VARCHAR(2000),
    ADD COLUMN listener_rule_arn VARCHAR(2000);
