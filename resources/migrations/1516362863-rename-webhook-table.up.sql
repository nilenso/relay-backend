ALTER TABLE fastspring_webhook_events RENAME TO payment_webhook_events;

ALTER TABLE payment_webhook_events
    ADD COLUMN source VARCHAR(100);

UPDATE payment_webhook_events SET source = 'fastspring';

ALTER TABLE payment_webhook_events
    ALTER COLUMN source SET NOT NULL;
