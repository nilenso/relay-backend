CREATE UNIQUE INDEX IF NOT EXISTS subscriptions_active_org_id_key
    ON subscriptions (org_id) WHERE status = 'active';
