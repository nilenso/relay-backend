CREATE UNIQUE INDEX IF NOT EXISTS orgs_active_unique_key
    ON orgs (subdomain) WHERE status = 'running';
ALTER TABLE orgs DROP CONSTRAINT IF EXISTS orgs_subdomain_key;
