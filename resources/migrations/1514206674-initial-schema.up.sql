CREATE TABLE orgs (
    id UUID NOT NULL PRIMARY KEY,
    name VARCHAR(300) NOT NULL,
    email VARCHAR(300) NOT NULL,
    subdomain VARCHAR(100) NOT NULL UNIQUE,
    status VARCHAR(25) NOT NULL,
    address TEXT,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);

CREATE TABLE subscriptions (
    id UUID NOT NULL PRIMARY KEY,
    org_id UUID NOT NULL REFERENCES orgs (id),
    fs_id VARCHAR(100) NOT NULL,
    status VARCHAR(25) NOT NULL,
    currency VARCHAR(10) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);

CREATE TABLE fastspring_webhook_events (
    id UUID NOT NULL PRIMARY KEY,
    event_json JSONB NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);
