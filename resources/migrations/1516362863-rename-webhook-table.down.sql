ALTER TABLE payment_webhook_events DROP COLUMN source;

ALTER TABLE payment_webhook_events RENAME TO fastspring_webhook_events;
