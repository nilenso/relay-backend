CREATE TABLE charge_attempts (
    id UUID NOT NULL PRIMARY KEY,
    org_id UUID NOT NULL REFERENCES orgs(id),
    subs_id UUID NOT NULL REFERENCES subscriptions(id),
    payment_id UUID REFERENCES payments(id),
    active_user_count INTEGER NOT NULL,
    amount NUMERIC(12,2) NOT NULL,
    period_start timestamp without time zone NOT NULL,
    period_end timestamp without time zone NOT NULL,
    status VARCHAR(100) NOT NULL,
    failure_reason VARCHAR (100),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);