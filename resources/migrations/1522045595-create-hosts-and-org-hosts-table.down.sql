ALTER TABLE orgs
  DROP COLUMN host_id,
  DROP COLUMN port,
  DROP COLUMN target_group_arn,
  DROP COLUMN listener_rule_arn;

DROP TABLE hosts;