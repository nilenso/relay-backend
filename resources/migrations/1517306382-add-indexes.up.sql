CREATE INDEX IF NOT EXISTS orgs_subdomain_status_key ON orgs(subdomain, status);
CREATE INDEX IF NOT EXISTS subscriptions_org_id_status_key ON subscriptions(org_id, status);