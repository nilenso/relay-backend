DROP INDEX orgs_active_unique_key;
CREATE UNIQUE INDEX orgs_active_unique_key
    ON orgs (subdomain) WHERE status = 'running';
