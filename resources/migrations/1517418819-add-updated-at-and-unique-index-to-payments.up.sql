ALTER TABLE payments
 ADD COLUMN updated_at timestamp without time zone NOT NULL,
 ADD CONSTRAINT unique_org_subs_order UNIQUE (org_id, subs_id, order_id);