DROP INDEX orgs_active_unique_key;
CREATE UNIQUE INDEX orgs_active_unique_key ON orgs (lower(subdomain))
    WHERE status in ('running', 'signup');
