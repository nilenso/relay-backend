ALTER TABLE payments
DROP COLUMN IF EXISTS updated_at,
DROP CONSTRAINT IF EXISTS unique_org_subs_order;