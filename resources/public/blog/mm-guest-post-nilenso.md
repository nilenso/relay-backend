
## Custom Mattermost from nilenso

- explain: what is nilenso? who are we and what do we do?
- describe nilenso's culture of f/oss: using & contributing
- create and link: https://nilenso.com/open-source.html ?
- alternatively, tell the story of our history of looking for open source projects to contribute to as consultants
  - Ashoka Survey
  - wonko, agrajag, etc.
  - JOSM
- briefly describe replacing Slack with Mattermost and creating Relay (link Relay guest post and/or Origin Story)
- explain the dovetail of Relay's Mattermost contributions and nilenso's Mattermost customizations
  - Relay contributes back to the Mattermost core
  - nilenso contributes consulting to the Mattermost ecosystem
