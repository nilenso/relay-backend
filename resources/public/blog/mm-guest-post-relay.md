## Relay is Hosted Mattermost

- meta: really focus on "hosted mattermost"
- why we moved from Slack => Mattermost
- link to the "Origin Story" on Medium: https://medium.com/@deobald/we-created-relay-by-escaping-slack-aedf0579a65f
- why we didn't choose Matrix
- who is Relay for? people who:
  - want hosted mattermost
  - care about privacy
  - want to use open source software
  - want cloud chat from a company who listens
  - want to support community
- what have we built so far?
  - https://gitlab.com/nilenso?filter=relay
- what will we build next? (link to https://trello.com/relayapp)
