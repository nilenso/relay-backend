
#hackers-india: kitty, neena, abhinav
Slack: sandy
Twitter
  - mike monteiro: https://medium.com/@monteiro/one-persons-history-of-twitter-from-beginning-to-end-5b41abed6c20

IRC
HipChat

2014 we used email, and IRC, maybe even the chat that comes with basecamp.
  - https://basecamp.com/retired/campfire
  - build an IRC client?
2015 we used hipchat for a bit
most of 2015, and 2016, we used slack
half way through 2017, we started using mattermost

why not slack?
  - privacy
  - cost in light of data mining (we like to pay for good software)


we should probably mention the transition from slack to mattermost though.
setup in less than a day (inc backups)
imported all the data initially
then used abnv's mirrorbot for a couple of months, so some people used slack on phone, etc
we then switched to matterbridge, a community built mirroring tool (worth mentioning because we'll prolly build on it later)
discussed pros and cons of mattermost and slack, then made the full switch to turn off slack
started recommending MM to others (import, hosting, monitoring, backups)
moved over all nilenso-public conversations (~100 people) to open relay successfully

- hugs => handshake => video chat bodylanguage => phone call => WhatsApp (slack/IRC/text) message => email => JIRA ticket => memo => time capsule => mission statement

https://dave.cheney.net/2017/04/11/why-slack-is-inappropriate-for-open-source-communications

when was the last time a text message made you cry?

sound and fury (gabbar, gojek)

email, coops:
- referendums: https://www.youtube.com/watch?v=uYTFCSZb3Kc&t=1586s
- on concrete jungles




feedback:
- add intro
- "keep Relay small" - how?
- could use more headers and whitespace. - jake
- maybe just ": Relay."
- "We will keep Relay small.  We will listen to you".
  - s/will// - keep the same present tense, no promises
