
# How we created Relay by escaping Slack

When was the last time a text message made you cry? The last time a sharply-worded email caused your eyes to widen and your heart to quicken? Has a JIRA ticket made your knee pits sweat in anticipatory delight?

Humans try to choose a communication medium based on the content of the message, the desired results upon delivery, the anticipated longevity of said message, and the intensity and intimacy of the interaction. Even when your world plays at the [utilitarian edges of the speed of light](https://www.youtube.com/watch?v=JEpsKnWZrJ8), as ours does, Computer Science and Physics lack satisfying terminology to distinguish between the faint and early Tuesday morning kiss from your entangled partner, which jostles you from a reality in which your role is that of a peg-legged sea captain with laser eyelashes and back into a reality where the "T" in Tuesday also stands for Tacos, and the sweaty handshake of an overcaffeinated client at a midday meeting smack dab between the jostling and the actual tacos. These messages occur at the same distance (zero) and for the same duration (one second) and yet they are worlds apart in their results and intimacy.

Our company, [nilenso](https://nilenso.com), is based in Bangalore with clients and colleagues on every continent. "What distance?" is answered for us: Earth. Anywhere on Earth.

Persistent messages like Github issues and Trello cards solve the distance problem and tend to be project-specific. As such, each message lasts as long as it needs to but is unlikely to outlive the project which contains it.

Ephemeral messages had a gap for us. Between video chats and email there was a chasm: chat.

- - -

Let's go back to 2013. Mike Monteiro was only halfway to [giving up on Twitter](https://medium.com/@monteiro/one-persons-history-of-twitter-from-beginning-to-end-5b41abed6c20) and he was probably still enjoying some of the online-meets-real-life magic dust that made Twitter a rehash of Internet Relay Chat (IRC) for many of us the previous decade (and this decade, mind you; we've hired three people from IRC). I'm not sure when it hit me that Twitter was federated IRC - "federated" at the message level, not the service level - but it seems painfully obvious in retrospect. On Twitter, you don't join channels and become part of a community, you "tag" your message with a channel (read: hashtag) and dump your message into it globally. A neat idea, but also one which prevents us from using Twitter for anything corporate. `#accounting` and `#conferences` can't be namespaced to nilenso and Twitter isn't open source so we can't set up our own instance. Not that we even had this idea at the time; everyone loves a little alternate history.

But we did have the idea that we wanted chat to happen _somewhere_. In 2013, there was competition for chat: IRC, in all its monospaced glory, competed directly with the universally acceptable bastardization of email as an unstructured chat protocol. The chat tools of yesteryear, Google Talk chief among them, were a joy to use even on mobile devices as long as the only communications network graph you cared about was two nodes connected by a single line. One-on-one, today special-cased as "Direct Message", was almost the only mode of operation. Almost the complete inverse of Twitter, group chat was not their forte.

By 2014, we had a long-standing `#nilenso` channel on Freenode, populated largely by me, kitty, and neena: nilenso's only IRC users. We struggled to convince anyone esle to join, to remember to keep their IRC client open, and to keep a bot up that would catalogue everything said in the channel. We tried [Campfire](https://basecamp.com/retired/campfire), HipChat, and even considered writing our own IRC clients and servers, but nothing took hold.

Then came Slack.

2015 and 2016 saw us heap love on Slack. It worked, mostly. Chat history was persisted. Unlike HipChat, it had mobile clients that didn't make me want to murder-suicide my phone with my own skull. Search sort of worked some of the time. Good enough! Sold! Everyone on Slack!

We even convinced our clients to try Slack, where we could. We were actually doing _sales_ for Slack and yet we knew, ultimately, we were paying close to $2000/year for the fanciest IRC client any of us had laid eyes on. Slack is little more than persistent IRC with a proprietary protocol. The UI is just _adorable_, though.

- - -

We could afford Slack. India is a "developing" nation. Bangalore is in South India but rent here can easily double a comparable apartment in Tokyo. And nilenso is headquartered in Bangalore but our rates are often too high for clients in London.

We can easily afford $2000/year for Slack. So why would we switch?

We are a software company. Slack is not software for software developers. It isn't open source, it isn't auditable, it isn't customizable, it isn't secure and it can't be made secure. Slack, Inc. has a Privacy Policy that would make Vladimir Putin choke on his tvorog and quail eggs. (tl;dr: Your data is Slack's data.) Slack is _Adorable By Default_ but if you don't like those defaults? Ok, Timothy. Get ready to [inject Javascript and CSS off a random gist, just to get a dark background](https://gist.github.com/DrewML/0acd2e389492e7d9d6be63386d75dd99) or suck it up. Slack is bloatware and could actually chew up multiple gigabytes of RAM. Yes, our developers all have 16 gigs of RAM but it feels a tad wasteful to throw out 20% on a bloody _chat client_.

Possibly the most frustrating aspect of Slack, however, didn't affect us directly: They have no respect for community. Between 2015 and 2018, almost every open source project, conference organizing committee, and non-profit has signed up for Slack - and it underserves them all. These organizations can rarely pay for Slack and often have thousands of users. If the Elixir-Lang community paid for Slack it would cost them upward of $1,000,000/year. ONE MILLION DOLLARS. Free Slack accounts have a 10,000 message limit. Slack still "works" as long as the community doesn't care about message history but without message history Slack isn't even persistent IRC. It's just IRC.

So we switched.

It started humbly. Abhinav spent a few hours setting up a Mattermost server, importing all our Slack data, ensuring backups worked, and writing [Mirrorbot](TODO) to keep Mattermost and Slack in sync while we kicked the tyres.

We ran Mirrorbot for a month while we debated Mattermost in the `#mattermost-vs-slack` channel. Then we switched to community-built [Matterbridge](TODO) and continued to debate. Debate what? Well, Mattermost wasn't as cute as Slack. The mobile clients had bugs. Did we really want to get off of Slack in the first place? Wasn't everyone else already on Slack? Were we just signing up to spread ourselves across multiple platforms?

As the months rolled on and our experience allayed our fears, we performed our final backups, moved our public channels into [Open Relay](TODO), and stopped paying for Slack. We started selling Mattermost to our friends the way we'd sold Slack to them two years prior... but self-hosted software just isn't easy or sexy in 2018. Businesses today want to outsource everything non-essential and "setup, host, monitor, and backup some chat software" isn't usually high on the list unless you run a chat software company. Since we were already doing it, we decided to become a chat software company: Relay was born.

Relay is hosted Mattermost. We will contribute all our changes back to Mattermost and open source anything new we build. We promise [your data is yours](TODO), your data is private. [We support communities.](TODO) We run an [open network](TODO). We will keep Relay small. We will listen to you.

What would you like to tell us?



--

Second Intro:

Slack is a prison. Incarceration here is as optional as jeans-and-a-t-shirt in a Bubble startup. Your morning coffee isn’t preceded by a soliloquy bereaving ties and khakis. You don’t scream into a screen of little red bubbles with numbers on them, pining for the simplicity of a singular, infinitely-replenishing Holy Ampulla of email. But as you react to a what’s-for-lunch poll with an avocado emoji with your butt nestled in $200 raw-denim jeans, there’s a voice that says: This wasn’t your choice.

But the social pressure which urges us to spend more hours talking about work than actually working is the road to the prison, not the prison itself.

Our jailers are our service providers. Slack, Inc. can read your company’s data and share it with its partners. Your company pays $8/month for the privilege of putting you in that spotlight.

Our company, nilenso, is an employee-owned software co-operative with clients and colleagues on every continent, and we’re building an open-source, respectful, your-data-is-yours Slack alternative: Relay.

Privacy and security are only half the story. For the rest, we have to time travel a half-decade to a time without Slack.
