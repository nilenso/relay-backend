
# The Third Era does not demand cryptocurrencies

- what prevents GAFA from amassing cryptocurrencies in the same way they have amassed cash?
- why did people want to build open protocols in the First Era? why does that motivation not apply now?
- open/closed is a sine wave, not eras based on decade-long events

response to:
https://medium.com/@cdixon/why-decentralization-matters-5e3f79f7638e

reference:
https://www.bloomberg.com/view/articles/2018-02-20/trading-is-a-good-way-to-set-a-price
