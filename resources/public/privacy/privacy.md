



----

# Nilenso Software LLP (Relay) Official Privacy Policy

Effective Date: February 6, 2018

## Who we are and what this is

Relay is a workplace communication tool for companies, non-profits, and other institutions to organise online chat. It is owned and operated by Nilenso Software LLP.

We take the private nature of your personal information very seriously, and are committed to protecting it. To do that, we’ve set up procedures to ensure that your information is handled responsibly and in accordance with applicable data protection and privacy laws. We’re grateful for your trust, and we’ll act that way.

This privacy policy describes what information we collect when you visit our website, sign up for a Relay account, use Relay client applications, or connect your Relay server to 3rd party services. This policy also describes how we use that information, and what choices we offer you to access, update, and control it.

To the best of our ability, this Privacy Policy describes how Relay provides you with [Notice, Choice, Access, Security, and Enforcement](https://en.wikipedia.org/wiki/FTC_fair_information_practice#Principles) regarding the information you store with us.


## The short version

Data in your organisation, accounts, and messages belongs to you. We collect your information only with your consent; we only collect the minimum amount of personal information that is necessary to fulfill the purpose of your interaction with us; we don't sell it to third parties; we will not share it without your permission or an instruction from law enforcement; and we only use it as this Privacy Statement describes.

If you're visiting us from the EU (or anywhere, really): we comply with the [General Data Protection Regulation (GDPR)](https://www.eugdpr.org).

Please read on for more detail.


## What information Relay collects and why

### Information from web browsers

If you're just browsing the website, we collect the same basic information that most websites collect. We use common internet technologies, such as cookies and web server logs. We collect this from everyone, whether they have an account or not.

We browser information we collect from everyone who visits our website includes: browser type, language preference, referring site, additional websites requested, and the date/time of each request. We also collect potentially personally-identifying information like Internet Protocol (IP) addresses.

### Why do we collect this?

We collect this information to better understand how our website visitors use Relay, and to monitor and protect the security of the website.


### Information from users with accounts

If you create an account, we require some basic information at the time of account creation. You will create your own user name and password, and we will ask you for a valid email account and the name of your organisation. You also have the option to give us more information if you want to, and this may include "User Personal Information."

"User Personal Information" is any information about one of our users which could, alone or together with other information, personally identify him or her. Information such as a user name and password, an email address, a real name, and a photograph are examples of "User Personal Information."

User Personal Information does not include aggregated, non-personally identifying information. We may use aggregated, non-personally identifying information to operate, improve, and optimize our website and service.


### Why do we collect this?

We need your User Personal Information to create your account, and to provide the services you request.

We use your User Personal Information to connect you to your organisation and other members of your organisation.

We will use your email address to communicate with you, if you've said that's okay, and only for the reasons you’ve said that’s okay.

We limit our use of your User Personal Information to the purposes listed in this Privacy Statement. If we need to use your User Personal Information for other purposes, we will ask your permission first.


## What information Relay does not collect

We do not intentionally collect sensitive personal information, such as social security numbers, genetic data, health information, or religious information. Although Relay does not request or intentionally collect any sensitive personal information, we acknowledge that you might store this kind of information using Relay, perhaps as messages to another user.  If you store any sensitive personal information on our servers, you are consenting to our storage of that information on our servers, which are in India (AWS Mumbai).

We do not collect information that is stored in your organisation's chats (messages/channels), settings, profiles, or any other free-form content inputs. Information in your organisation, accounts, and messages belongs to you. You are responsible for this information as well as ensuring that it complies with our [End User License Agreement](https://www.relay-chat.com/eula). Employees of Nilenso Software LLP do not access organisations' private chats unless required to for security, maintenance, or support reasons with the consent of the organisation owner.

If your organisation is public (similar to [Open Relay](https://open.relay-chat.com)) anyone (including us) may view the contents of public channels you have created within that organisation.

If you are a minor (less than 18 years of age) you cannot use Relay, own a Relay organisation, or have an account on Relay's servers.


## How we share the information we collect

We do not share, sell, rent, or trade User Personal Information with third parties for any reason.

We do not disclose User Personal Information outside Relay, except in the situations listed in this section or in the section below on **Compelled Disclosure**.

We may share User Personal Information with your permission, so we can perform services you have requested.

We may share User Personal Information with a limited number of third-party vendors who process it on our behalf. Our vendors perform services such as payment processing.

### How we share your non-personally identifying information

We are not actively collecting non-personally identifying information from users or organisations in Relay, but this term describes all messaging data you enter into Relay and store on Relay servers. As such, there are times when we will be required to share this information. Sharing of this information mirrors how we share User Personal Information, for two reasons: (1) This is your data, and we will only do with it as you request. (2) In aggregate, even non-personally identifying information may be used to identify an individual by way of a user fingerprint via implicit identifiers [1](http://www.cs.yale.edu/homes/ramki/mobicom07.pdf), [2](http://ieeexplore.ieee.org/document/6393552) or [device fingerprint](https://en.wikipedia.org/wiki/Device_fingerprint). We value you privacy and we also understand that even this data is sensitive. Therefore:

We do not share, sell, rent, or trade non-personally identifying information, either.

We do not disclose non-personally identifying information outside Relay, except in the situations listed in this section or in the section below on **Compelled Disclosure**.

We may share non-personally identifying information with your permission.

We may share non-personally identifying information with a limited number of third-party vendors who process it on our behalf. Our vendors perform services such as payment processing.


## Our use of cookies and tracking

### Cookies

We use cookies to keep you logged into our website and remember your preferences. Cookies are also used in the web, mobile, and desktop Relay client applications to keep your user account logged in.

Certain pages on our site may set other third party cookies. For example, we may embed content, such as videos, from another site that sets a cookie. While we try to minimize these third party cookies, we can’t always control what cookies this third party content sets.


## Google Analytics

We use Google Analytics as a third party tracking service, but we don’t use it to track you individually or collect your User Personal Information. We use Google Analytics to collect information about how our website performs.


## Tracking

We do not track your online browsing activity on other online services over time and we do not permit third-party services to track your activity on our site beyond our basic Google Analytics tracking.


## How Relay secures your information

Relay takes all measures reasonably necessary to protect User Personal Information from unauthorized access, alteration, or destruction; maintain data accuracy; and help ensure the appropriate use of User Personal Information. We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it.

No method of transmission, or method of electronic storage, is 100% secure. Therefore, we cannot guarantee its absolute security.


## Relay's global privacy practices

Information that we collect will be stored and processed in India in accordance with this privacy statement. However, we understand that we have users from different countries and regions with different privacy expectations, and we try to meet those needs.

We provide the same standard of privacy protection to all our users around the world, regardless of their country of origin or location.


## Resolving complaints

If you have concerns about the way Relay is handling your User Personal Information, please let us know immediately. You may email us directly at privacy@relay-chat.com with the subject line "Privacy Concerns". We will respond within 45 days at the latest.


## How we respond to Compelled Disclosure

Relay may disclose personally-identifying information or other information we collect about you to law enforcement in response to a valid subpoena, court order, warrant, or similar government order.

In complying with court orders and similar legal processes, Relay strives for transparency. When permitted, we will make a reasonable effort to notify users of any disclosure of their information, unless we are prohibited by law or court order from doing so.


## How you can access and control the information we collect

You can add, edit, and remove information from your account using any of the Relay client applications. Complete deletion of organisations or accounts can be facilitated by us by emailing support@relay-chat.com.

### Data retention and deletion

Relay will retain User Personal Information for as long as your account is active or as needed to provide you services.


## How we communicate with you

We will use your email address to communicate with you, if you've said that's okay, and only for the reasons you’ve said that’s okay.


## Changes to our Privacy Statement

Although most changes are likely to be minor, Relay may change our Privacy Statement from time to time.

We will provide notification to Users of material changes to this Privacy Statement through our Website by posting a notice on our home page or sending email to the email address specified in your Relay account.


## How to contact us

If you have any questions about this policy or our site in general, please contact us at team@relay-chat.com.

Our Data Protection Officer is Abhinav Sarkar.

Written Inquiries can be sent to:

Nilenso Software LLP
Attn: Abhinav Sarkar, DPO
#105 10th Cross
Indiranagar Stage 1
Bangalore, Karnataka
India - 560038


## License

This Privacy Statement is licensed under the [Creative Commons Zero license](https://creativecommons.org/publicdomain/zero/1.0/).
