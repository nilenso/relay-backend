.PHONY: all test clean

# HELP sourced from https://gist.github.com/prwhite/8168133

# Add help text after each target name starting with '\#\#'
# A category can be added with @category

HELP_FUNC = \
    %help; \
    while(<>) { \
        if(/^([a-z0-9_-]+):.*\#\#(?:@(\w+))?\s(.*)$$/) { \
            push(@{$$help{$$2}}, [$$1, $$3]); \
        } \
    }; \
    print "usage: make [target]\n\n"; \
    for ( sort keys %help ) { \
        print "$$_:\n"; \
        printf("  %-30s %s\n", $$_->[0], $$_->[1]) for @{$$help{$$_}}; \
        print "\n"; \
    }

help:           ##@miscellaneous Show this help.
	@perl -e '$(HELP_FUNC)' $(MAKEFILE_LIST)


# BUILD #############

uberjar: ##@build create the uberjar to deploy
	lein do clean, sass4clj once, uberjar

run-uberjar-api: ##@build run the uberjar
	source secrets.sh && java -jar target/relay-backend-0.1.0-SNAPSHOT-standalone.jar api

# DEV #############

run-api: ##@dev run the api through lein
	source secrets.sh && lein trampoline run api

run: ##@dev run figwheel and api through lein
	lein trampoline sass4clj auto &
	make run-api &
	lein figwheel dev

stop: ##@dev stop all processes started by `run`
	ps aux | grep java | grep relay-backend | awk '{print $$2}' | xargs -n 1 kill -15

migrate: ##@dev migrate db to latest schema
	lein run migrate

rollback: ##@dev rollback last migration
	lein run rollback

test: ##@dev run lein tests
	lein test

dockerize: ##@build create the docker image to deploy
	docker build -t relay-backend .

run-docker-api: ##@dev run docker container
	docker run --name relay-backend \
		--env-file ${RELAY_VAULT_DIR}/ops-secrets/relay-backend.staging.secrets.sh \
		--restart unless-stopped \
		-v ${RELAY_VAULT_DIR}:/etc/relay-vault \
		-d --publish 7020:7020 --publish 7021:7021 relay-backend

stop-docker-api:  ##@dev stop docker container
	docker stop relay-backend
	docker rm relay-backend
