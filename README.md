# relay-backend

The backend for relay-chat.com.

## Usage

To start the backend service,
```
LOG_LEVEL_RELAY_BACKEND=DEBUG lein trampoline run api
```
starts on http://localhost:7020, with an nrepl server on 7021.

## Dev
### DB setup
```
initdb -D relay-backend-db
pg_ctl -D relay-backend-db -l relay-backend-db.log start
createdb relay_dev
```
and pass DB creds as env while starting the service

To develop frontend, run
```
lein figwheel dev
```

## License

Copyright © 2017-2018 Nilenso Software LLP

Distributed under the GNU AFFERO GENERAL PUBLIC LICENSE Version 3.
