;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.signup-test
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [clojure.test :refer :all]
            [relay-backend.db.core :as db]
            [relay-backend.db.org :as org]
            [relay-backend.fixtures :as fixtures]
            [relay-backend.instance :as instance]
            [relay-backend.signup :as signup]
            [relay-backend.spec :as rspec]
            [medley.core :refer [random-uuid]]
            [relay-backend.factories :as factories]
            [relay-backend.db.org :as db-org]
            [relay-backend.db.host :as db-host]))

(use-fixtures :once fixtures/db-init)
(use-fixtures :each fixtures/db-reset)

(deftest test-signup
  (testing "If signup fails, retrying should work"
    (let [first-org-id (random-uuid)
          retry-org-id (random-uuid)
          _host        (db/with-transaction [tx]
                         (db-host/create tx (factories/host)))]

      (with-redefs [instance/safely-spin-up (constantly false)]
        (signup/signup {:id        first-org-id
                        :name      "foo"
                        :subdomain "test-signup"})
        (db/with-transaction [tx]
          (is (= "failed" (:status (org/find tx {:id first-org-id}))))))

      (with-redefs [instance/safely-spin-up (constantly true)]
        (signup/signup {:id        retry-org-id
                        :name      "foo"
                        :subdomain "test-signup"})
        (let [org (db/with-transaction [tx] (org/find tx {:id retry-org-id}))]
          (is (= (:running db-org/org-status) (:status org))))))))

(deftest subdomain-available-test
  (testing "Valid subdomain is unavailable if there is a running org with the same"
    (let [org  (factories/org {:subdomain "running-sub" :status (:running db-org/org-status)})
          _org (db/with-transaction [tx] (db-org/create tx org))]
      (is (false? (signup/subdomain-available? "running-sub")))))
  (testing "Valid subdomain is unavailable if there is a singup org with the same"
    (let [org  (factories/org {:subdomain "signup-sub" :status (:signup db-org/org-status)})
          _org (db/with-transaction [tx] (db-org/create tx org))]
      (is (false? (signup/subdomain-available? "signup-sub")))))
  (testing "Valid subdomain is unavailable if there is a singup org with the same (case-insensitive)"
    (let [org  (factories/org {:subdomain "signup-sub" :status (:signup db-org/org-status)})
          _org (db/with-transaction [tx] (db-org/create tx org))]
      (is (false? (signup/subdomain-available? (str/upper-case "signup-sub"))))))
  (testing "Valid subdomain is available if there is a failed org with the same"
    (let [org  (factories/org {:subdomain "failed-sub" :status (:failed db-org/org-status)})
          _org (db/with-transaction [tx] (db-org/create tx org))]
      (is (true? (signup/subdomain-available? "failed-sub")))))
  (testing "Valid subdomain is available if there is a no org with the same"
    (is (true? (signup/subdomain-available? "noorg-sub")))))

(deftest signup-param-validity-test
  (testing "Invalid subdomain if constraints fail"
    (is (s/valid? ::rspec/subdomain "asddd0099-77"))
    (is (s/valid? ::rspec/subdomain "uppercase"))
    (is (s/valid? ::rspec/subdomain "ubxxx"))
    (is (s/valid? ::rspec/subdomain (apply str (repeat 50 "x"))))
    (is (not (s/valid? ::rspec/subdomain "")))
    (is (not (s/valid? ::rspec/subdomain "UPPERCaSe")))
    (is (not (s/valid? ::rspec/subdomain "ub")))
    (is (not (s/valid? ::rspec/subdomain (apply str (repeat 51 "x")))))
    (is (not (s/valid? ::rspec/subdomain "0ub")))
    (is (not (s/valid? ::rspec/subdomain "u_b")))
    (is (not (s/valid? ::rspec/subdomain "ub-")))
    (is (not (s/valid? ::rspec/subdomain "u%b")))
    (is (not (s/valid? ::rspec/subdomain "u$b")))
    (is (not (s/valid? ::rspec/subdomain "www")))
    (is (not (s/valid? ::rspec/subdomain "blog")))
    (is (not (s/valid? ::rspec/subdomain "backend")))
    (is (not (s/valid? ::rspec/subdomain "api")))
    (is (not (s/valid? ::rspec/subdomain "status"))))
  (testing "Invalid username if constraints fail"
    (is (s/valid? ::rspec/username "sss"))
    (is (s/valid? ::rspec/username (apply str (repeat 64 "x"))))
    (is (s/valid? ::rspec/username "sxx-_.ssss"))
    (is (not (s/valid? ::rspec/username "")))
    (is (not (s/valid? ::rspec/username "ss")))
    (is (not (s/valid? ::rspec/username (apply str (repeat 65 "x")))))
    (is (not (s/valid? ::rspec/username "9sxx")))
    (is (not (s/valid? ::rspec/username "#sxx")))
    (is (not (s/valid? ::rspec/username "xasdd$ff")))
    (is (not (s/valid? ::rspec/username "all")))
    (is (not (s/valid? ::rspec/username "channel")))
    (is (not (s/valid? ::rspec/username "matterbot"))))
  (testing "Invalid password if constraints fail"
    (is (s/valid? ::rspec/password "ssssssss"))
    (is (s/valid? ::rspec/password "sssssAAs,._+:@%/-"))
    (is (not (s/valid? ::rspec/password "")))
    (is (not (s/valid? ::rspec/password "ssss")))
    (is (not (s/valid? ::rspec/password "ssss;sss")))
    (is (not (s/valid? ::rspec/password "ssss#sss")))
    (is (not (s/valid? ::rspec/password (apply str (repeat 65 "x")))))))
