;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.middleware-test
  (:require [clojure.test :refer :all]
            [relay-backend.server.middleware :refer [indexify-uri]]))

(deftest indexify-uri-test
  (is (= "/index.html" (indexify-uri "/")))
  (is (= "/eula/index.html" (indexify-uri "/eula")))
  (is (= "/privacy/index.html" (indexify-uri "/privacy/")))
  (is (= "/base.css" (indexify-uri "/base.css")))
  (is (= "/js/base.js" (indexify-uri "/js/base.js"))))


