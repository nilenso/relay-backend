;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.fixtures
  (:require [clojure.test :refer :all]
            [relay-backend.db.core :as db]
            [mount.core :as mount]
            [relay-backend.config :as config]
            [relay-backend.db-migration :as db-migration]
            [relay-backend.util.scheduler :as scheduler]))

(defn truncate-tables []
  (db/with-transaction [tx]
    (db/truncate tx "subscriptions, payments, orgs, payment_webhook_events, charge_attempts, hosts")))

(defn mount-test-config []
  (-> (mount/only [#'config/config])
      (mount/swap {#'config/config (config/make-config "test.config.edn")})
      (mount/start (mount/except [#'scheduler/pool]))))

(defn db-reset [f]
  (truncate-tables)
  (f)
  (truncate-tables))

(defn db-init [f]
  (mount-test-config)
  (mount/start #'relay-backend.db.core/datasource)
  (db-migration/migrate)
  (f)
  (mount/stop))
