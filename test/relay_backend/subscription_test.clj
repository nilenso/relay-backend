;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.subscription-test
  (:require [clojure.test :refer :all]
            [java-time :as jt]
            [relay-backend.config :as config]
            [relay-backend.db.charge-attempt :as db-ca]
            [relay-backend.db.core :as db]
            [relay-backend.db.org :as db-org]
            [relay-backend.db.org :as org]
            [relay-backend.db.payment :as db-payment]
            [relay-backend.db.subscription :as db-subs]
            [relay-backend.factories :as factories]
            [relay-backend.fastspring :as fastspring]
            [relay-backend.fixtures :as fixtures]
            [relay-backend.subscription :as subs]
            [relay-backend.subscription :as subscription]
            [relay-backend.util.time :as ut]))

(use-fixtures :once fixtures/db-init)
(use-fixtures :each fixtures/db-reset)

(deftest calc-charge-amount-test
  (testing "Calculates full charge amount for a subscripion"
    (let [today             (jt/instant)
          subscription      (factories/subscription {:created-at (-> (jt/offset-date-time)
                                                                     (jt/minus (jt/months 1))
                                                                     (jt/instant))})
          days-created      (jt/time-between (:created-at subscription) today :days)
          active-user-count 100
          charge-amount     (subscription/calc-charge-amount active-user-count days-created)]
      (is (= 100.00 charge-amount))))

  (testing "Calculates prorata charge amount for a subscripion created less than a month ago"
    (let [days-created      15
          _subscription     (factories/subscription {:created-at (-> (jt/offset-date-time)
                                                                     (jt/minus (jt/days days-created))
                                                                     (jt/instant))})
          active-user-count 100
          charge-amount     (subscription/calc-charge-amount active-user-count days-created)
          expected-charge   (subscription/floor-currency
                              (* (/ days-created (ut/days-in-previous-month))
                                 active-user-count
                                 (config/subscription :monthly-active-user-price)))]
      (is (= expected-charge charge-amount)))))

(defn inc-call-count [a k]
  (swap! a #(update % k inc)))

(defn set-call-arg [a k v]
  (swap! a #(update % k (constantly v))))

(defmacro with-external-redefs [[active-user-count calculated-amount call-count] & body]
  `(let [~call-count (atom {:fs-update-subs             0
                            :fs-update-subs-id          nil
                            :fs-charge-subs             0
                            :fs-charge-subs-id          nil
                            :subs-get-active-user-count 0})]
     (with-redefs [fastspring/update-subscription     (fn [subs-id# _#]
                                                        (inc-call-count ~call-count :fs-update-subs)
                                                        (set-call-arg ~call-count :fs-update-subs-id subs-id#)
                                                        true)
                   fastspring/charge-subscription     (fn [subs-id#]
                                                        (inc-call-count ~call-count :fs-charge-subs)
                                                        (set-call-arg ~call-count :fs-charge-subs-id subs-id#)
                                                        true)
                   subscription/get-active-user-count (fn [_#]
                                                        (inc-call-count ~call-count :subs-get-active-user-count)
                                                        ~active-user-count)
                   subscription/calc-charge-amount    (fn [_# _#] ~calculated-amount)]
       ~@body)))

(deftest can-charge-subscription-to-a-org
  (testing "Org has valid subscription to be charged at full price"
    (let [org               (db/with-transaction [tx] (db-org/create tx (factories/org)))
          org-id            (:id org)
          subs              (db/with-transaction [tx]
                              (db-subs/create tx (factories/subscription {:org-id org-id})))
          subs-id           (:id subs)
          active-user-count 1
          calculated-amount 100.00M
          clock             (jt/mock-clock 0 "UTC")]
      (with-external-redefs [active-user-count calculated-amount call-count]
        (jt/with-clock clock
          (subscription/charge-subscription org-id)
          (let [charge-attempt          (db/with-transaction [tx] (db-ca/find tx {:org-id org-id}))
                expected-charge-attempt {:org-id            org-id
                                         :subs-id           subs-id
                                         :active-user-count active-user-count
                                         :amount            calculated-amount
                                         :period-start      (:created-at subs)
                                         :period-end        (jt/instant)}]
            (is (= expected-charge-attempt (select-keys charge-attempt
                                                        [:org-id
                                                         :subs-id
                                                         :active-user-count
                                                         :amount
                                                         :period-start
                                                         :period-end])))
            (is (= {:fs-update-subs             1
                    :fs-charge-subs             1
                    :subs-get-active-user-count 1
                    :fs-update-subs-id          (:subs-id subs)
                    :fs-charge-subs-id          (:subs-id subs)}
                   @call-count)))))))

  (testing "Does not charge subscription if org does not exists"
    (let [org               (factories/org)
          org-id            (:id org)
          active-user-count 1
          calculated-amount 100.00M
          clock             (jt/mock-clock 0 "UTC")]
      (with-external-redefs [active-user-count calculated-amount call-count]
        (jt/with-clock clock
          (subscription/charge-subscription org-id)
          (let [charge-attempt (db/with-transaction [tx] (db-ca/find tx {:org-id org-id}))]
            (is (nil? charge-attempt))
            (is (= {:fs-update-subs             0
                    :fs-charge-subs             0
                    :subs-get-active-user-count 0
                    :fs-update-subs-id          nil
                    :fs-charge-subs-id          nil}
                   @call-count)))))))

  (testing "Does not charge subscription if subscription for org does not exists"
    (let [org               (db/with-transaction [tx] (db-org/create tx (factories/org)))
          org-id            (:id org)
          active-user-count 1
          calculated-amount 100.00M
          clock             (jt/mock-clock 0 "UTC")]
      (with-external-redefs [active-user-count calculated-amount call-count]
        (jt/with-clock clock
          (subscription/charge-subscription org-id)
          (let [charge-attempt (db/with-transaction [tx] (db-ca/find tx {:org-id org-id}))]
            (is (nil? charge-attempt))
            (is (= {:fs-update-subs             0
                    :fs-charge-subs             0
                    :subs-get-active-user-count 0
                    :fs-update-subs-id          nil
                    :fs-charge-subs-id          nil}
                   @call-count)))))))

  (testing "Does not charge subscription if a payment has been made since the begining of the month"
    (let [clock (jt/mock-clock 0 "UTC")]
      (jt/with-clock clock
        (let [org               (db/with-transaction [tx] (db-org/create tx (factories/org)))
              org-id            (:id org)
              subs              (db/with-transaction [tx]
                                  (db-subs/create tx (factories/subscription {:org-id org-id})))
              _                 (jt/advance-clock! clock (jt/plus (jt/days 31)))
              _payment          (db/with-transaction [tx]
                                  (db-payment/create
                                    tx (factories/payment {:org-id org-id :subs-id (:subs-id subs)})))
              active-user-count 1
              calculated-amount 100.00M]
          (with-external-redefs [active-user-count calculated-amount call-count]
            (subscription/charge-subscription org-id)
            (let [charge-attempt (db/with-transaction [tx] (db-ca/find tx {:org-id org-id}))]
              (is (nil? charge-attempt))
              (is (= {:fs-update-subs             0
                      :fs-charge-subs             0
                      :subs-get-active-user-count 0
                      :fs-update-subs-id          nil
                      :fs-charge-subs-id          nil}
                     @call-count))))))))

  (testing "Does not charge subscription if a charge-attempt exists for the subscription"
    (let [org               (db/with-transaction [tx] (db-org/create tx (factories/org)))
          org-id            (:id org)
          subs              (db/with-transaction [tx]
                              (db-subs/create tx (factories/subscription {:org-id org-id})))
          charge-attempt    (db/with-transaction [tx]
                              (db-ca/create
                                tx (factories/charge-attempt {:org-id org-id :subs-id (:id subs)})))
          active-user-count 1
          calculated-amount 100.00M]
      (with-external-redefs [active-user-count calculated-amount call-count]
        (subscription/charge-subscription org-id)
        (is (= {:fs-update-subs             0
                :fs-charge-subs             0
                :subs-get-active-user-count 0
                :fs-update-subs-id          nil
                :fs-charge-subs-id          nil}
               @call-count))))))

(deftest charge-subscriptions
  (let [_orgs (db/with-transaction [tx]
                (doseq [org (repeatedly 5 factories/org)]
                  (org/create tx org)))
        clock (jt/mock-clock (ut/first-day-of-month) "UTC")]
    (testing "Charges subscriptions to all the orgs if the date is first of the month"
      (let [call-count (atom {:charge-subscription 0})]
        (jt/with-clock clock
          (with-redefs [subscription/charge-subscription (fn [_] (inc-call-count call-count :charge-subscription))]
            (do
              (subs/charge-subscriptions)
              (is (= 5 (:charge-subscription @call-count))))))))

    (testing "Does not charge subscriptions to any org it is not first of the month"
      (let [call-count (atom {:charge-subscription 0})]
        (jt/with-clock clock
          (jt/advance-clock! clock (jt/plus (jt/hours 3)))
          (with-redefs [subscription/charge-subscription (fn [_] (inc-call-count call-count :charge-subscription))]
            (do
              (subs/charge-subscriptions)
              (is (= 5 (:charge-subscription @call-count))))))))))

