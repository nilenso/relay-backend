;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.fastspring-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]
            [cheshire.core :as json]
            [medley.core :refer [uuid]]
            [relay-backend.fastspring :as fastspring]
            [relay-backend.fixtures :as fixtures]
            [relay-backend.db.core :as db]
            [relay-backend.db.payment :as payment]
            [relay-backend.db.charge-attempt :as charge-attempt]
            [relay-backend.db.org :as org]
            [clojure.set :as set]
            [relay-backend.util.map :as umap]
            [relay-backend.db.subscription :as subs]
            [relay-backend.db.subscription :as subscription]
            [relay-backend.factories :as factories]))

(use-fixtures :once fixtures/db-init)
(use-fixtures :each fixtures/db-reset)

(def payment-keys {:order                 :order-id
                   :currency              :charge-currency
                   :total                 :charge-total
                   :tax                   :charge-tax
                   :totalInPayoutCurrency :payout-total
                   :taxInPayoutCurrency   :payout-tax})

(def payment-transformers {:total                 bigdec
                           :tax                   bigdec
                           :totalInPayoutCurrency bigdec
                           :taxInPayoutCurrency   bigdec})

(defn read-json-resource [resource-path]
  (-> resource-path
      (io/resource)
      (slurp)
      (json/decode true)))


(deftest handle-order-completed-event-test
  (let [event           (read-json-resource "webhook_order_completed_response.json")
        org-id          (-> event :data :tags :org-id (uuid))
        _org            (db/with-transaction [tx]
                          (org/create tx (factories/org {:id        org-id
                                                         :subdomain "test"
                                                         :name      "test"})))

        webhook-payment (-> event
                            :data
                            (select-keys (keys payment-keys))
                            (umap/transform payment-transformers)
                            (set/rename-keys payment-keys))]
    (fastspring/handle-events {:params {:events [event]}})
    (let [payment (db/with-transaction [tx]
                    (payment/find tx {:order-id (:order event)
                                      :org-id   org-id}))]
      (is (not (nil? payment)))
      (is (= webhook-payment (select-keys payment (vals payment-keys)))))))

(deftest handle-multiple-order-completed-event-test
  (let [event  (read-json-resource "webhook_order_completed_response.json")
        org-id (-> event :data :tags :org-id (uuid))
        _org   (db/with-transaction [tx]
                 (org/create tx (factories/org {:id        org-id
                                                :subdomain "test"
                                                :name      "test"})))]
    (fastspring/handle-events {:params {:events [event]}})
    (fastspring/handle-events {:params {:events [event]}})
    (let [payments (db/with-transaction [tx]
                     (payment/list tx {:order-id (:order event)
                                       :org-id   org-id}))]
      (is (= 1 (count payments))))))

(deftest handles-new-subscription-activated-event
  (let [event  (read-json-resource "webhook_subscription_activated_response.json")
        org-id (-> event :data :tags :org-id (uuid))
        _org   (db/with-transaction [tx]
                 (org/create tx (factories/org {:id        org-id
                                                :name      "Test Org"
                                                :subdomain "testdomain"})))
        {:keys [subscription state currency]} (:data event)
        subs   {:org-id   org-id
                :subs-id  subscription
                :status   state
                :currency currency}]
    (fastspring/handle-events {:params {:events [event]}})
    (let [saved-subs (db/with-transaction [tx] (subs/find tx {:org-id org-id}))]
      (is (= subs (dissoc saved-subs :id :created-at :updated-at))))))

(deftest handles-repeat-subscription-activated-event
  (let [event  (read-json-resource "webhook_subscription_activated_response.json")
        org-id (-> event :data :tags :org-id (uuid))
        _org   (db/with-transaction [tx]
                 (org/create tx (factories/org {:id        org-id
                                                :name      "Test Org"
                                                :subdomain "testdomain"})))
        {:keys [subscription state currency]} (:data event)
        subs   {:org-id   org-id
                :subs-id  subscription
                :status   state
                :currency currency}]
    (fastspring/handle-events {:params {:events [event]}})
    (fastspring/handle-events {:params {:events [event]}})
    (let [all-subs   (db/with-transaction [tx] (subs/list tx {:org-id org-id}))
          saved-subs (first all-subs)]
      (is (= 1 (count all-subs)))
      (is (= subs (select-keys saved-subs (keys subs)))))))

(deftest handles-subscription-charge-completed-event-test
  (let [event              (read-json-resource "webhook_subscription_charge_completed_response.json")
        org-id             (-> event :data :subscription :tags :org-id (uuid))
        subs-id            (-> event :data :subscription :id)
        _db-org            (db/with-transaction [tx]
                             (org/create tx (factories/org {:id        org-id
                                                            :subdomain "test"
                                                            :name      "test"})))
        db-subs            (db/with-transaction [tx]
                             (subs/create tx (factories/subscription {:org-id  org-id
                                                                      :subs-id subs-id})))
        _db-charge-attempt (db/with-transaction [tx]
                             (charge-attempt/create tx (factories/charge-attempt {:org-id  org-id
                                                                                  :subs-id (:id db-subs)})))
        webhook-payment    (-> event
                               :data
                               :order
                               (select-keys (keys payment-keys))
                               (umap/transform payment-transformers)
                               (set/rename-keys payment-keys))]
    (fastspring/handle-events {:params {:events [event]}})
    (let [payment        (db/with-transaction [tx]
                           (payment/find tx {:order-id (-> event :data :order :order)
                                             :org-id   org-id}))
          charge-attempt (db/with-transaction [tx]
                           (charge-attempt/find tx {:org-id org-id :subs-id (:id db-subs)}))]
      (testing "Creates a payment record in the db for the org and subscription"
        (is (= webhook-payment (select-keys payment (vals payment-keys)))))
      (testing "Updates the charge attempt status to success"
        (is (= (:success charge-attempt/charge-attempt-status) (:status charge-attempt)))
        (is (= (:id payment) (:payment-id charge-attempt)))
        (is (nil? (:failure-reason charge-attempt)))))))

(deftest handles-subscription-charge-failed-event-test
  (let [event    (read-json-resource "webhook_subscription_charge_failed_response.json")
        org-id   (-> event :data :subscription :tags :org-id (uuid))
        subs-id  (-> event :data :subscription :id)
        _org     (db/with-transaction [tx] (org/create tx (factories/org {:id        org-id
                                                                          :subdomain "test"
                                                                          :name      "test"})))
        db-subs  (db/with-transaction [tx]
                   (subscription/create tx (factories/subscription {:org-id org-id :subs-id subs-id})))
        _db-ca   (db/with-transaction [tx]
                   (charge-attempt/create tx (factories/charge-attempt {:org-id  org-id
                                                                        :subs-id (:id db-subs)})))
        expected {:org-id         org-id
                  :subs-id        (:id db-subs)
                  :status         "failed"
                  :failure-reason "EXPIRED_CARD"}]
    (fastspring/handle-events {:params {:events [event]}})
    (let [charge-attempt (db/with-transaction [tx]
                           (charge-attempt/find tx {:org-id  org-id
                                                    :subs-id (:id db-subs)}))]
      (is (= expected (select-keys charge-attempt (keys expected)))))))
