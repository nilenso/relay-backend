;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.factories
  (:require [clojure.test :refer :all]
            [java-time :as jt]
            [medley.core :refer [random-uuid]]
            [relay-backend.db.org :as db-org]
            [relay-backend.config :as config]))

(defn subscription
  ([]
   (let [now (jt/instant)]
     {:org-id     (random-uuid)
      :subs-id    (str "subs-id" (rand-int 100))
      :status     "active"
      :currency   "INR"
      :created-at now
      :updated-at now}))
  ([args] (merge (subscription) args)))

(defn org
  ([]
   (let [now   (jt/instant)
         index (rand-int 100)]
     {:id                (random-uuid)
      :name              (str "org name" index)
      :subdomain         (str "subdomain" index)
      :status            (:running db-org/org-status)
      :address           "Org Address"
      :created-at        now
      :updated-at        now
      :bot-client-id     "bot-client-id"
      :bot-client-secret "bot-client-secret"}))
  ([args] (merge (org) args)))

(defn payment
  ([]
   (let [now (jt/instant)]
     {:order-id        "order-id"
      :org-id          (random-uuid)
      :subs-id         "subs-id"
      :charge-total    100M
      :charge-tax      10M
      :charge-currency "INR"
      :payout-total    100M
      :payout-tax      10M
      :payout-currency "INR"
      :completed       true
      :invoice-url     "http://invoice.url.com"
      :created-at      now
      :updated-at      now}))
  ([args] (merge (payment) args)))

(defn charge-attempt
  ([]
   (let [now (jt/instant)]
     {:org-id            (random-uuid)
      :subs-id           (random-uuid)
      :active-user-count 1
      :amount            1
      :period-start      now
      :period-end        now
      :status            "attempted"}))
  ([args] (merge (charge-attempt) args)))

(defn host
  ([]
   (let [now (jt/instant)]
     {:id             (random-uuid)
      :instance-id    (str "instance-id" (rand-int 100))
      :private-ipaddr (str "127.0.0." (rand-int 100))
      :capacity       3
      :created-at     now
      :updated-at     now}))
  ([args] (merge (host) args)))
