;
;     relay-backend. The backend for relay-chat.com.
;     Copyright (C) 2017-2018 Nilenso Software LLP.
;
;     This program is free software: you can redistribute it and/or modify
;     it under the terms of the GNU Affero General Public License as published
;     by the Free Software Foundation, either version 3 of the License, or
;     (at your option) any later version.
;
;     This program is distributed in the hope that it will be useful,
;     but WITHOUT ANY WARRANTY; without even the implied warranty of
;     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;     GNU Affero General Public License for more details.
;
;     You should have received a copy of the GNU Affero General Public License
;     along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;

(ns relay-backend.host-test
  (:require [clojure.test :refer :all])
  (:require [medley.core :refer [random-uuid]]
            [relay-backend.aws :as aws]
            [relay-backend.host :as host]
            [relay-backend.db.core :as db]
            [relay-backend.db.host :as db-host]
            [relay-backend.factories :as factories]
            [relay-backend.fixtures :as fixtures]
            [relay-backend.db.org :as db-org]
            [clojure.tools.logging :as log]))


(use-fixtures :once fixtures/db-init)
(use-fixtures :each fixtures/db-reset)

(deftest select-host-to-deploy-org-test
  (testing "Returns the details host with least number of running orgs"
    (let [orgs           (repeatedly 5 factories/org)
          _db-orgs       (doseq [org orgs]
                           (db/with-transaction [tx] (db-org/create tx org)))
          hosts          (repeatedly 2 factories/host)
          _db-hosts      (doseq [host hosts]
                           (db/with-transaction [tx] (db-host/create tx host)))
          parts          (split-at 2 orgs)
          _db-org-hosts  (doseq [org-hosts (partition 2 (interleave hosts parts))]
                           (db/with-transaction [tx]
                             (doseq [o (last org-hosts)]
                               (db-org/update tx (:id o) {:host-id (:id (first org-hosts))
                                                          :port    (rand-int 1000)}))))
          org            (factories/org)
          _db-org        (db/with-transaction [tx] (db-org/create tx org))
          expected-host  (first hosts)
          expected-value (-> expected-host
                             (dissoc :created-at :updated-at)
                             (assoc :id (:id expected-host)
                                    :num-orgs (count (first parts))))]
      (is (= expected-value (dissoc (host/select-host-to-deploy-org org)
                                    :occupied-ports
                                    :selected-port
                                    :created-at
                                    :updated-at))))))

(deftest select-host-with-available-capacity
  (testing "Returns host where number of orgs is less than capacity"
    (let [orgs           (repeatedly 7 factories/org)
          _db-orgs       (doseq [org orgs]
                           (db/with-transaction [tx] (db-org/create tx org)))
          host1          (factories/host {:capacity 2})
          host2          (factories/host {:capacity 10})
          hosts          [host1 host2]
          _db-hosts      (doseq [host hosts]
                           (db/with-transaction [tx] (db-host/create tx host)))
          parts          (split-at 3 orgs)
          _db-org-hosts  (doseq [org-hosts (partition 2 (interleave hosts parts))]
                           (db/with-transaction [tx]
                             (doseq [o (last org-hosts)]
                               (db-org/update tx (:id o) {:host-id (:id (first org-hosts))
                                                          :port    (rand-int 1000)}))))
          org            (factories/org)
          _db-org        (db/with-transaction [tx] (db-org/create tx org))
          expected-host  host2
          expected-value (-> expected-host
                             (dissoc :created-at :updated-at)
                             (assoc :num-orgs (count (second parts))))]
      (is (= expected-value (dissoc (host/select-host-to-deploy-org org)
                                    :created-at :updated-at :occupied-ports :selected-port))))))

(deftest create-target-group-test
  (testing "Creates a target group on AWS and updates the org-host record with target-group arn"
    (let [org          (factories/org)
          host         (factories/host)
          _db-org      (db/with-transaction [tx] (db-org/create tx org))
          _db-host     (db/with-transaction [tx] (db-host/create tx host))
          _db-org-host (db/with-transaction [tx] (db-org/update tx (:id org) {:host-id (:id host)
                                                                              :port    1234}))
          call-count   (atom {:aws-create-target-group 0})]
      (with-redefs [aws/create-target-group
                    (fn [_host _subdomain]
                      (swap! call-count #(update % :aws-create-target-group inc))
                      {:target-group-arn "target-group-arn"})]
        (host/create-target-group host org)
        (let [updated-org (db/with-transaction [tx] (db-org/find tx {:id (:id org)}))]
          (is (= 1 (:aws-create-target-group @call-count)))
          (is (= "target-group-arn" (:target-group-arn updated-org))))))))

(deftest register-forwarding-rules-test
  (testing "Creates listener rules on AWS and updates the org-host record with the listener-rules"
    (let [org          (factories/org)
          host         (factories/host)
          _db-org      (db/with-transaction [tx] (db-org/create tx org))
          _db-host     (db/with-transaction [tx] (db-host/create tx host))
          _db-org-host (db/with-transaction [tx] (db-org/update tx (:id org) {:host-id (:id host)
                                                                              :port    1234}))
          target-arn   "target-group-arn"
          call-count   (atom {:aws-register-forwarding-rule 0})]
      (with-redefs [aws/register-forwarding-rule
                    (fn [_subdomain _target-group-arn listener-arn _priority]
                      (swap! call-count #(update % :aws-register-forwarding-rule inc))
                      {:rule-arn (str "rule-arn-" listener-arn)})]
        (host/register-forwarding-rule org target-arn)
        (let [updated-org (db/with-transaction [tx] (db-org/find tx {:id (:id org)}))]
          (is (= 1 (:aws-register-forwarding-rule @call-count)))
          (is (= "rule-arn-https-listener-arn" (:listener-rule-arn updated-org))))))))

(deftest setup-routing
  (testing "Calls all functions needed to setup routing for an org"
    (let [target-group-arn "target-group-arn"
          call-count       (atom {:host-create-target-group              0
                                  :host-register-forwarding-rule         0
                                  :aws-register-instance-to-target-group 0})
          org              (factories/org)
          host             (factories/host)]
      (with-redefs [host/create-target-group
                    (fn [_host _org]
                      (swap! call-count #(update % :host-create-target-group inc))
                      {:target-group-arn target-group-arn})
                    host/register-forwarding-rule
                    (fn [_org _target-group]
                      (swap! call-count #(update % :host-register-forwarding-rule inc))
                      true)
                    aws/register-instance-to-target-group
                    (fn [_host _target-group]
                      (swap! call-count #(update % :aws-register-instance-to-target-group inc))
                      true)]
        (host/setup-routing host org)
        (is (= {:host-create-target-group              1
                :host-register-forwarding-rule         1
                :aws-register-instance-to-target-group 1}
               @call-count))))))